package ru.gildor.hackernews.android;

import dagger.Component;
import dagger.android.AndroidInjectionModule;
import ru.gildor.hackernews.android.core.scopes.AppScope;

@AppScope
@Component(modules = {
        StubAppModule.class,
        StubNetworkModule.class,
        ActivityBindingModule.class,
        AndroidInjectionModule.class
})
public interface StubAppComponent extends AppComponent {
    @Component.Builder
    abstract class Builder extends AppComponent.Builder {
    }
}
