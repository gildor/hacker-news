package ru.gildor.hackernews.android.features.comments;

import android.content.Intent;
import android.os.Bundle;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import org.junit.Rule;
import org.junit.Test;
import ru.gildor.hackernews.android.R;
import ru.gildor.hackernews.android.features.stories.StubStory;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.assertTrue;
import static ru.gildor.hackernews.android.test.RecyclerViewItemCountAssertion.withItemCount;

public class CommentsListActivityTest {
    @Rule
    public ActivityTestRule<CommentsListActivity> rule =
            new ActivityTestRule<>(CommentsListActivity.class, false, false);
    private StubStory story = new StubStory();

    @Test
    public void onCreate() throws Exception {
        rule.launchActivity(getIntent());
        onView(withId(R.id.recycler)).check(withItemCount(3));
    }

    @Test
    public void onSaveInstanceState() {
        rule.launchActivity(getIntent());
        final Bundle outState = new Bundle();
        final CommentsListActivity activity = rule.getActivity();
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                activity.onSaveInstanceState(outState);
                assertTrue(outState.containsKey(CommentsListActivityModule.COMMENTS_STATE));
            }
        });
    }

    private Intent getIntent() {
        return CommentsListActivity.createIntent(InstrumentationRegistry.getContext(), story);
    }
}