package ru.gildor.hackernews.android.core.providers.impl;

import android.content.Context;
import android.support.test.InstrumentationRegistry;

import org.junit.Test;

import static org.junit.Assert.assertFalse;

public class DateUtilsDateFormatterTest {

    @Test
    public void getRelativeDate() throws Exception {
        Context context = InstrumentationRegistry.getTargetContext();
        String relativeDate = new DateUtilsDateFormatter(context)
                .getRelativeDate(System.currentTimeMillis() / 1000);

        assertFalse("Relative date result is empty", relativeDate.isEmpty());

    }

}