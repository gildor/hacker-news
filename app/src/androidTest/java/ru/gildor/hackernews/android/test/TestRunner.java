package ru.gildor.hackernews.android.test;

import android.app.Application;
import android.content.Context;
import android.support.test.runner.AndroidJUnitRunner;
import ru.gildor.hackernews.android.TestApp;

@SuppressWarnings("unused")
public class TestRunner extends AndroidJUnitRunner {
    @Override
    public Application newApplication(ClassLoader cl, String className, Context context)
            throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        return super.newApplication(cl, TestApp.class.getName(), context);
    }
}