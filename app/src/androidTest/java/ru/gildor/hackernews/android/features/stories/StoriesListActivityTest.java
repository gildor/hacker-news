package ru.gildor.hackernews.android.features.stories;

import android.os.Bundle;
import android.support.test.rule.ActivityTestRule;
import org.junit.Rule;
import org.junit.Test;
import ru.gildor.hackernews.android.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.assertTrue;
import static ru.gildor.hackernews.android.test.RecyclerViewItemCountAssertion.withItemCount;

public class StoriesListActivityTest {
    @Rule
    public ActivityTestRule<StoriesListActivity> rule =
            new ActivityTestRule<>(StoriesListActivity.class, false);

    @Test
    public void onCreate() throws Exception {
        onView(withId(R.id.recycler)).check(withItemCount(3));
    }

    @Test
    public void onSaveInstanceState() {
        final Bundle outState = new Bundle();
        final StoriesListActivity activity = rule.getActivity();
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                activity.onSaveInstanceState(outState);
                assertTrue(outState.containsKey(StoriesListActivityModule.STORIES_STATE));
            }
        });
    }

}