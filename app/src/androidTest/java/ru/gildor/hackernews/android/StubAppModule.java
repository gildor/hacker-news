package ru.gildor.hackernews.android;

import dagger.Binds;
import dagger.Module;
import ru.gildor.hackernews.android.core.AppModule;
import ru.gildor.hackernews.android.core.scopes.AppScope;

@Module
public interface StubAppModule extends AppModule {
    @SuppressWarnings("unused")
    @Binds
    @AppScope
    AppComponent provideAppComponent(StubAppComponent application);
}
