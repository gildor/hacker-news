package ru.gildor.hackernews.android;

import android.support.test.rule.ActivityTestRule;
import dagger.android.AndroidInjector;
import org.junit.Rule;
import org.junit.Test;
import ru.gildor.hackernews.android.features.stories.StoriesListActivity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class AppTest {
    @Rule
    public ActivityTestRule<StoriesListActivity> rule =
            new ActivityTestRule<>(StoriesListActivity.class);

    @Test
    public void packageName() throws Exception {
        App app = (App) rule.getActivity().getApplicationContext();
        String packageName = app.getPackageName();
        assertEquals("ru.gildor.hackernews.android", packageName);
    }

    @Test
    public void injector() throws Exception {
        App app = (App) rule.getActivity().getApplicationContext();
        AndroidInjector<App> injector = app.applicationInjector();
        assertNotNull(injector);
        assertNotNull(app.component);
    }
}