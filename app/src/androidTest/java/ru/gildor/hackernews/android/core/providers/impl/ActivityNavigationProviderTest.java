package ru.gildor.hackernews.android.core.providers.impl;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentMatcher;
import ru.gildor.hackernews.android.features.comments.CommentsListActivity;
import ru.gildor.hackernews.android.features.stories.StoriesListActivity;
import ru.gildor.hackernews.android.features.stories.StubStory;

import static android.support.test.espresso.intent.Intents.intending;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasExtraWithKey;
import static org.hamcrest.core.AllOf.allOf;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;
import static ru.gildor.hackernews.android.features.comments.CommentsListActivityModule.COMMENT_ARG;

public class ActivityNavigationProviderTest {
    private static String url = "http://example.net/";
    @Rule
    public IntentsTestRule<StoriesListActivity> rule =
            new IntentsTestRule<>(StoriesListActivity.class);
    private ActivityNavigationProvider provider;

    @Before
    public void setUp() throws Exception {
        provider = new ActivityNavigationProvider(rule.getActivity());
    }

    @Test
    public void openComments() throws Exception {
        provider.openComments(new StubStory());
        intending(allOf(hasExtraWithKey(COMMENT_ARG), hasComponent(CommentsListActivity.class.toString())));
    }

    @Test
    public void navigateUp() throws Exception {
        Activity mock = mock(Activity.class);
        ActivityNavigationProvider nav = new ActivityNavigationProvider(mock);
        nav.navigateUp();
        verify(mock).onBackPressed();
    }

    @SuppressWarnings("deprecation")
    @Test
    public void openUrl() throws Exception {
        Activity mock = mock(Activity.class);
        Resources res = mock(Resources.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            when(res.getColor(anyInt(), any(Resources.Theme.class))).thenReturn(0);
        }
        when(res.getColor(anyInt())).thenReturn(0);
        when(mock.getResources()).thenReturn(res);
        ActivityNavigationProvider nav = new ActivityNavigationProvider(mock);
        nav.openUrl(url);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            verify(mock).startActivity(argThat(new ArgumentMatcher<Intent>() {
                @Override
                public boolean matches(Intent intent) {
                    return checkIntent(intent);
                }
            }), nullable(Bundle.class));
        } else {
            verify(mock).startActivity(argThat(new ArgumentMatcher<Intent>() {
                @Override
                public boolean matches(Intent intent) {
                    return checkIntent(intent);
                }
            }));
        }
    }

    private boolean checkIntent(Intent intent) {
        return url.equals(intent.getDataString()) && Intent.ACTION_VIEW.equals(intent.getAction());
    }

}