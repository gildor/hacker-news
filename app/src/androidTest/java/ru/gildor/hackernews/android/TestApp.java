package ru.gildor.hackernews.android;

import dagger.android.AndroidInjector;

public class TestApp extends App {
    @Override
    protected AndroidInjector<App> applicationInjector() {
        return DaggerStubAppComponent.builder().create(this);
    }
}
