package ru.gildor.hackernews.android.core.providers.impl;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SpanTextFormatterTest {

    private SpanTextFormatter formatter;

    @Before
    public void setUp() throws Exception {
        Context context = InstrumentationRegistry.getTargetContext();
        formatter = new SpanTextFormatter(context);
    }

    @Test
    public void formatUrl() throws Exception {
        SpannableString url = (SpannableString) formatter.formatUrl("url");
        ForegroundColorSpan[] fspan = url.getSpans(0, url.length(), ForegroundColorSpan.class);
        assertEquals(1, fspan.length);
        RelativeSizeSpan[] rspan = url.getSpans(0, url.length(), RelativeSizeSpan.class);
        assertEquals(1, rspan.length);
    }

    @Test
    public void concatSpans() throws Exception {
        CharSequence result = formatter.concatSpans(
                new SpannableString("one"),
                new SpannableString("-"),
                new SpannableString("two")
        );
        assertEquals("one-two", result.toString());
    }

    @Test
    public void fromHtml() throws Exception {
        CharSequence url = formatter.fromHtml("a<br/>b");
        assertEquals("a\nb", url.toString());
    }

}