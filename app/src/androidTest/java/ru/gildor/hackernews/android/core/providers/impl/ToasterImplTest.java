package ru.gildor.hackernews.android.core.providers.impl;

import android.support.test.rule.ActivityTestRule;
import org.junit.Rule;
import org.junit.Test;
import ru.gildor.hackernews.android.features.stories.StoriesListActivity;

public class ToasterImplTest {

    @Rule
    public ActivityTestRule<StoriesListActivity> rule =
            new ActivityTestRule<>(StoriesListActivity.class);

    @Test
    public void showToast() throws Exception {
        final String message = "toast!";
        final StoriesListActivity activity = rule.getActivity();

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new ToasterImpl(activity).showToast(message);
            }
        });

    }

}