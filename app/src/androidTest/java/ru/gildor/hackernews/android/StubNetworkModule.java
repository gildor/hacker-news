package ru.gildor.hackernews.android;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Observable;
import ru.gildor.hackernews.android.core.scopes.AppScope;
import ru.gildor.hackernews.android.features.stories.StubStory;
import ru.gildor.hackernews.api.HackerNewsApi;
import ru.gildor.hackernews.api.models.Comment;
import ru.gildor.hackernews.api.models.Story;

import java.util.Arrays;
import java.util.List;

/**
 * Module for network components.
 * Better to avoid use here any components related to Application,
 * better provide configuration objects instead
 */
@Module
class StubNetworkModule {
    @Provides
    @AppScope
    HackerNewsApi provideHackerNewsApi() {
        return new HackerNewsApi() {
            @Override
            public Observable<List<Story>> loadTopStories(int limit) {
                if (limit == 0) {
                    return Observable.empty();
                }
                return Observable.just(Arrays.asList(
                        createStory(),
                        createStory(),
                        createStory()
                ));
            }

            private Story createStory() {
                return new StubStory();
            }

            private Comment createComment() {
                return new StubStory();
            }

            @Override
            public Observable<List<Comment>> loadStoryComments(Story story) {
                return loadStoryComments(story.kids());
            }

            @Override
            public Observable<List<Comment>> loadStoryComments(List<Long> commentIds) {
                if (commentIds.isEmpty()) {
                    return Observable.empty();
                }
                return Observable.just(Arrays.asList(
                        createComment(),
                        createComment(),
                        createComment()
                ));
            }

            @Override
            public Observable<Story> loadStory(long storyId) {
                return null;
            }

            @Override
            public Observable<Comment> loadComment(long commentId) {
                return null;
            }

            @Override
            public Observable<List<Comment>> loadChildComments(Comment comment) {
                return null;
            }
        };
    }
}
