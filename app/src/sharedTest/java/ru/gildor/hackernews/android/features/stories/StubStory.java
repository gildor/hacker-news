package ru.gildor.hackernews.android.features.stories;

import java.util.Arrays;
import java.util.List;

import ru.gildor.hackernews.api.models.Comment;
import ru.gildor.hackernews.api.models.Story;

public class StubStory implements Story, Comment {

    @Override
    public String title() {
        return "StubStory";
    }

    @Override
    public int descendants() {
        return 13;
    }

    @Override
    public int score() {
        return 42;
    }

    @Override
    public String url() {
        return "story://url";
    }

    @Override
    public long id() {
        return 123;
    }

    @Override
    public String by() {
        return "Story Author";
    }

    @Override
    public List<Long> kids() {
        return Arrays.asList(1L, 2L, 3L);
    }

    @Override
    public long time() {
        return 1490000000;
    }

    @Override
    public Type type() {
        return Type.STORY;
    }

    @Override
    public Long parent() {
        return 123L;
    }

    @Override
    public String text() {
        return "comment text";
    }

    @Override
    public boolean deleted() {
        return false;
    }
}
