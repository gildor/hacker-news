package ru.gildor.hackernews.android.core.state;

import android.os.Bundle;
import android.support.annotation.Nullable;
import io.reactivex.Maybe;

public interface StateRepository<T> {
    void updateState(T state);

    void saveState(@Nullable Bundle bundle);

    void restoreState(@Nullable Bundle state);

    Maybe<T> consumeState();
}
