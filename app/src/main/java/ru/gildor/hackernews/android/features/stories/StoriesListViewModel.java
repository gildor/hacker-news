package ru.gildor.hackernews.android.features.stories;

import io.reactivex.Observable;
import ru.gildor.hackernews.android.R;
import ru.gildor.hackernews.android.core.bindings.SimpleListViewModel;
import ru.gildor.hackernews.android.core.providers.ResourceProvider;
import ru.gildor.hackernews.android.core.providers.Toaster;

import javax.inject.Inject;
import java.util.List;

/**
 * ViewModel for Screen with list of stories
 */
public class StoriesListViewModel extends SimpleListViewModel<StoryViewModel> {

    private ResourceProvider res;

    @Inject
    StoriesListViewModel(
            Observable<List<StoryViewModel>> storiesObservable,
            ResourceProvider res,
            Toaster toaster
    ) {
        super(storiesObservable, toaster);
        this.res = res;
    }

    @Override
    public String title() {
        return res.getString(R.string.app_name);
    }

}
