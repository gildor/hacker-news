package ru.gildor.hackernews.android.core.rx;

import io.reactivex.functions.Consumer;
import ru.gildor.hackernews.android.core.state.StateRepository;

public class StateConsumer<T> implements Consumer<T> {
    private StateRepository<T> stateRepository;

    public StateConsumer(StateRepository<T> stateRepository) {
        this.stateRepository = stateRepository;
    }

    @Override
    public void accept(T state) throws Exception {
        stateRepository.updateState(state);
    }
}