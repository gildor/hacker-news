package ru.gildor.hackernews.android.core.scopes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Qualifier;

/**
 * Dagger qualifier to mark DataBindingView initialized with model
 */
@Qualifier
@Retention(RetentionPolicy.RUNTIME)
public @interface InitializedBinding {
}