package ru.gildor.hackernews.android;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import ru.gildor.hackernews.android.core.ActivityModule;
import ru.gildor.hackernews.android.core.scopes.ActivityScope;
import ru.gildor.hackernews.android.features.comments.CommentsListActivity;
import ru.gildor.hackernews.android.features.comments.CommentsListActivityModule;
import ru.gildor.hackernews.android.features.stories.StoriesListActivity;
import ru.gildor.hackernews.android.features.stories.StoriesListActivityModule;

/**
 * This module and method need only to map activity and activity module for Dagger code generation
 * <p>
 * "unused" suppressed because this method used only compile time
 */
@SuppressWarnings("unused")
@Module
abstract class ActivityBindingModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = {StoriesListActivityModule.class, ActivityModule.class})
    abstract StoriesListActivity contributeStoreActivityInjector();

    @ActivityScope
    @ContributesAndroidInjector(modules = {CommentsListActivityModule.class, ActivityModule.class})
    abstract CommentsListActivity contributeCommentsActivityInjector();
}
