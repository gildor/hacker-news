package ru.gildor.hackernews.android.core.bindings;

import android.databinding.ObservableField;
import ru.gildor.hackernews.android.core.providers.Toaster;

import java.util.Collections;
import java.util.List;

import static android.databinding.Observable.OnPropertyChangedCallback;

public class ErrorPropertyChangedCallback<T> extends OnPropertyChangedCallback {
    private final ObservableField<String> error;
    private final ObservableField<List<T>> list;
    private Toaster toaster;

    ErrorPropertyChangedCallback(Toaster toaster, ObservableField<String> error, ObservableField<List<T>> list) {
        super();
        this.toaster = toaster;
        this.error = error;
        this.list = list;
    }

    @Override
    public void onPropertyChanged(android.databinding.Observable sender, int propertyId) {
        List<T> list = this.list.get();
        list = list == null ? Collections.<T>emptyList() : list;
        if (error.get() != null && list.size() > 0) {
            toaster.showToast(error.get());
        }
    }
}
