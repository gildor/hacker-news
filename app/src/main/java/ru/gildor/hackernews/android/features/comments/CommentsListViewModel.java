package ru.gildor.hackernews.android.features.comments;

import io.reactivex.Observable;
import ru.gildor.hackernews.android.core.bindings.SimpleListViewModel;
import ru.gildor.hackernews.android.core.providers.NavigationProvider;
import ru.gildor.hackernews.android.core.providers.Toaster;
import ru.gildor.hackernews.android.core.scopes.ActivityScope;
import ru.gildor.hackernews.api.models.Story;

import javax.inject.Inject;
import java.util.List;

@ActivityScope
public class CommentsListViewModel extends SimpleListViewModel<CommentViewModel> {
    private Story story;
    private NavigationProvider nav;

    @Inject
    CommentsListViewModel(
            Story story,
            Observable<List<CommentViewModel>> listObservable,
            NavigationProvider nav,
            Toaster toaster
    ) {
        super(listObservable, toaster);
        this.story = story;
        this.nav = nav;
    }

    @Override
    public String title() {
        return story.title();
    }

    public void onNavigationClick() {
        nav.navigateUp();
    }

}

