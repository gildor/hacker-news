package ru.gildor.hackernews.android.features.comments;

public interface CommentViewModel {
    String text();

    String info();
}
