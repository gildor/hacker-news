package ru.gildor.hackernews.android.core.rx;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;

/**
 * Implementation of rxjava Function to transform List of elements of type T to a list of elements of type R
 * Client must override abstract method transform()
 *
 * @param <T> input list item type
 * @param <R> output list item type
 */
public abstract class ListTransformer<T, R> implements Function<List<T>, List<R>> {
    @Override
    public final List<R> apply(@NonNull List<T> in) throws Exception {
        if (in == null) throw new NullPointerException("Parameter in cannot be empty");

        ArrayList<R> out = new ArrayList<>();
        for (int i = 0; i < in.size(); i++) {
            out.add(transform(i, in.get(i)));
        }
        return out;
    }

    /**
     * @param index position of item in list
     * @param item  list item
     * @return item for output list
     */
    protected abstract R transform(int index, T item);
}
