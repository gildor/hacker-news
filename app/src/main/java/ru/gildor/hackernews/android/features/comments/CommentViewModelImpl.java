package ru.gildor.hackernews.android.features.comments;

import javax.inject.Inject;

import ru.gildor.hackernews.android.core.providers.DateFormatter;
import ru.gildor.hackernews.android.core.scopes.AppScope;
import ru.gildor.hackernews.api.models.Comment;

@AppScope
class CommentViewModelImpl implements CommentViewModel {
    private final Comment story;
    private DateFormatter date;

    @Inject
    CommentViewModelImpl(
            Comment comment,
            DateFormatter date
    ) {
        this.story = comment;
        this.date = date;
    }

    @Override
    public String text() {
        return story.text();
    }

    @Override
    public String info() {
        return story.by() + " " + date.getRelativeDate(story.time());
    }
}
