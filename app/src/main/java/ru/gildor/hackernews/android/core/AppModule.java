package ru.gildor.hackernews.android.core;

import android.content.Context;
import dagger.Binds;
import dagger.Module;
import ru.gildor.hackernews.android.App;
import ru.gildor.hackernews.android.core.providers.*;
import ru.gildor.hackernews.android.core.providers.impl.*;
import ru.gildor.hackernews.android.core.scopes.AppScope;

/**
 * Module for Application related dependencies
 * <p>
 * We use @Binds annotation here to map implementation to dependency and avoid boilerplate code
 * See https://google.github.io/dagger/faq.html#binds
 * <p>
 * This interface used only in compile time, so we suppress "unused" warning
 */
@SuppressWarnings("unused")
@Module
public interface AppModule {
    @Binds
    @AppScope
    ResourceProvider provideResourceProvider(ContextResourceProvider provider);

    @Binds
    @AppScope
    TextFormatter provideTextFormatter(SpanTextFormatter formatter);

    @Binds
    @AppScope
    DateFormatter provideDateFormatter(DateUtilsDateFormatter formatter);

    @Binds
    @AppScope
    Toaster provideToaster(ToasterImpl toaster);

    @Binds
    @AppScope
    MovementMethodProvider provideMovementMethod(LinkMovementMethodProvider movementMethod);

    /**
     * Use application context as default Context implementation
     */
    @Binds
    @AppScope
    Context provideContext(App application);
}
