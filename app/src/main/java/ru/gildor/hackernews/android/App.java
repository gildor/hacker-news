package ru.gildor.hackernews.android;

import android.databinding.DataBindingUtil;
import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import timber.log.Timber;

import javax.inject.Inject;

/**
 * Extends DaggerApplication to allow inject Activities and another Android components
 */
public class App extends DaggerApplication {
    @Inject
    AppComponent component;

    @Override
    public void onCreate() {
        super.onCreate();

        initLogger();
        initDataBindings();
    }

    /**
     * Enable logging
     */
    private void initLogger() {
        //noinspection StatementWithEmptyBody
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        } else {
            //Add release builds crash and logs reporting here
        }
    }

    /**
     * Set default DataBindingComponent with binding adapters
     */
    private void initDataBindings() {
        DataBindingUtil.setDefaultComponent(component);
    }

    /**
     * Create our application from this point (will be called inside super.onCreate())
     */
    @Override
    protected AndroidInjector<App> applicationInjector() {
        return DaggerAppComponent
                .builder()
                .create(this);
    }

}
