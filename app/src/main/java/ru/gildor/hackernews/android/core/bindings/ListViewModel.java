package ru.gildor.hackernews.android.core.bindings;

import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;

import java.util.List;

/**
 * Interface for "Screen with list" ViewModels
 *
 * @param <T>
 */
public interface ListViewModel<T> {
    String title();

    ObservableBoolean isLoading();

    ObservableField<List<T>> list();

    ObservableField<String> error();

    void refresh();
}
