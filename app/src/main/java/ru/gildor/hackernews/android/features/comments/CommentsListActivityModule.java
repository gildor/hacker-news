package ru.gildor.hackernews.android.features.comments;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import dagger.Module;
import dagger.Provides;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import ru.gildor.hackernews.android.R;
import ru.gildor.hackernews.android.core.scopes.ActivityScope;
import ru.gildor.hackernews.android.core.scopes.InitializedBinding;
import ru.gildor.hackernews.android.core.state.StateRepository;
import ru.gildor.hackernews.android.core.state.StateRepositoryImpl;
import ru.gildor.hackernews.android.databinding.ActivityCommentsListBinding;
import ru.gildor.hackernews.api.HackerNewsApi;
import ru.gildor.hackernews.api.models.Comment;
import ru.gildor.hackernews.api.models.Story;

import java.util.List;


@Module
public class CommentsListActivityModule {

    public static final String COMMENT_ARG = "story";
    static final String COMMENTS_STATE = "comments_state";

    @Provides
    Activity providesActivity(CommentsListActivity activity) {
        return activity;
    }

    @Provides
    @ActivityScope
    Story providesStory(CommentsListActivity activity) {
        Story story = (Story) activity.getIntent().getSerializableExtra(COMMENT_ARG);
        if (story == null) {
            throw new IllegalArgumentException("No Story found");
        }
        return story;
    }

    @Provides
    Observable<List<CommentViewModel>> provideCommentsListObservable(
            Story story,
            HackerNewsApi api,
            CommentsListActivity activity,
            CommentsListTransformer transformer,
            final StateRepository<List<Comment>> stateRepository
    ) {
        return Observable
                .concat(
                        stateRepository.consumeState().toObservable(),
                        api.loadStoryComments(story)
                                .doOnNext(new Consumer<List<Comment>>() {
                                    @Override
                                    public void accept(List<Comment> stories) throws Exception {
                                        stateRepository.updateState(stories);
                                    }
                                })
                )
                .subscribeOn(Schedulers.io())
                .firstOrError()
                .toObservable()
                .compose(activity.<List<Comment>>bindToLifecycle())
                .map(transformer)
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Provides
    @ActivityScope
    StateRepository<List<Comment>> provideStateRepository() {
        return new StateRepositoryImpl<>(COMMENTS_STATE);
    }

    @Provides
    @ActivityScope
    ActivityCommentsListBinding provideCommentsListBinding(CommentsListActivity activity) {
        return DataBindingUtil.setContentView(
                activity,
                R.layout.activity_comments_list
        );
    }

    @Provides
    @ActivityScope
    @InitializedBinding
    ActivityCommentsListBinding provideInitializedCommentsListBinding(
            ActivityCommentsListBinding binding,
            CommentsListViewModel model
    ) {
        binding.setModel(model);
        return binding;
    }
}
