package ru.gildor.hackernews.android.core.providers.impl;

import android.content.Context;
import android.support.annotation.NonNull;

import javax.inject.Inject;

import ru.gildor.hackernews.android.core.providers.ResourceProvider;

public final class ContextResourceProvider implements ResourceProvider {
    private final Context context;

    @Inject
    ContextResourceProvider(Context context) {
        this.context = context.getApplicationContext();
    }

    @NonNull
    @Override
    public String getString(int id) {
        return context.getString(id);
    }

    @NonNull
    @Override
    public String getString(int id, Object... formatArgs) {
        return context.getString(id, formatArgs);
    }

    @NonNull
    @Override
    public String getQuantityString(int id, int quantity, Object... formatArgs) {
        return context.getResources().getQuantityString(id, quantity, formatArgs);
    }

}
