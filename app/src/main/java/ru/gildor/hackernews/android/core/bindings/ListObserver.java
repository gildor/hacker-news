package ru.gildor.hackernews.android.core.bindings;

import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;

import java.util.List;

/**
 * Helper class, implementation of Rx callback Observer.
 * Automatically set list (onNext) and loading states to passed ObservableFields
 *
 * @param <T> list item type
 */
public class ListObserver<T> implements Observer<List<T>> {
    private final ObservableField<List<T>> list;
    private final ObservableBoolean isLoading;
    private ObservableField<String> error;

    ListObserver(ObservableField<List<T>> list, ObservableBoolean isLoading, ObservableField<String> error) {
        this.list = list;
        this.isLoading = isLoading;
        this.error = error;
    }

    @Override
    public void onSubscribe(Disposable d) {
        //Do nothing
        isLoading.set(true);
    }

    @Override
    public void onNext(List<T> list) {
        this.list.set(list);
        error.set(null);
        isLoading.set(false);
    }

    @Override
    public void onError(Throwable e) {
        Timber.e(e, "List downloading error");
        isLoading.set(false);
        error.set(e.getMessage());
    }

    @Override
    public void onComplete() {
        isLoading.set(false);
    }
}
