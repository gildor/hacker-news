package ru.gildor.hackernews.android.core.bindings;

import android.databinding.BindingAdapter;
import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;

import java.util.List;

import javax.inject.Inject;

import ru.gildor.hackernews.android.core.adapters.ListBindingAdapter;
import ru.gildor.hackernews.android.core.scopes.AppScope;

/**
 * DataBinding adapters to use only with RecyclerView
 */
@AppScope
public class RecyclerBindingAdapter {
    @Inject
    RecyclerBindingAdapter() {
    }

    /**
     * Creates RecyclerView.Adapter from list of items
     * <p>
     * Explicitly extend generic Object to wildcard,
     * otherwise DataBinding cannot use adapter with generics polymorphism
     *
     * @param view RecyclerView to show list of items
     * @param item layout id with binding adapter for list item
     * @param list list of items for Recycler view
     */
    @SuppressWarnings("TypeParameterExplicitlyExtendsObject")
    @BindingAdapter(value = {"item", "list"})
    public void setListAdapter(RecyclerView view, @LayoutRes int item,
                               List<? extends Object> list) {
        view.setAdapter(new ListBindingAdapter<>(list, item));
    }
}
