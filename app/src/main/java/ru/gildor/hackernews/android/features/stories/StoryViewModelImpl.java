package ru.gildor.hackernews.android.features.stories;

import android.support.annotation.IntRange;
import android.support.annotation.Nullable;

import java.net.URI;
import java.util.List;

import ru.gildor.hackernews.android.R;
import ru.gildor.hackernews.android.core.providers.DateFormatter;
import ru.gildor.hackernews.android.core.providers.NavigationProvider;
import ru.gildor.hackernews.android.core.providers.ResourceProvider;
import ru.gildor.hackernews.android.core.providers.TextFormatter;
import ru.gildor.hackernews.api.models.Story;

/**
 * Implementation of StoryViewModel based on Story and Android resources
 */
class StoryViewModelImpl implements StoryViewModel {

    private final CharSequence title;
    private final String relativeDate;
    private int order;
    private Story story;
    private ResourceProvider res;
    private NavigationProvider nav;

    StoryViewModelImpl(
            @IntRange(from = 1) int order,
            Story story,
            ResourceProvider res,
            NavigationProvider nav,
            TextFormatter formatter,
            DateFormatter dateFormatter
    ) {
        this.order = order;
        this.story = story;
        this.res = res;
        this.nav = nav;
        //Cache relative date, because it's quite long operation for list
        relativeDate = dateFormatter.getRelativeDate(story.time());
        //Format title and append url to title
        this.title = formatter.concatSpans(
                story.title(),
                " ",
                formatUrl(res, formatter, story.url())
        );
    }

    private CharSequence formatUrl(ResourceProvider res, TextFormatter formatter, @Nullable String url) {
        if (url == null) {
            return "";
        } else {
            return formatter.formatUrl(
                    res.getString(
                            R.string.url_suffix,
                            URI.create(url).getHost()
                    )
            );
        }
    }

    @Override
    public int order() {
        return order;
    }

    @Override
    public CharSequence title() {
        return title;
    }

    @Override
    public String info() {
        return res.getString(
                R.string.story_info,
                res.getQuantityString(R.plurals.points, story.score(), story.score()),
                story.by(),
                relativeDate
        );
    }

    @Override
    public void openComments() {
        nav.openComments(story);
    }

    @Override
    public void openUrl() {
        nav.openUrl(story.url());
    }

    @Override
    public int commentsCount() {
        List<Long> kids = story.kids();
        return kids == null ? 0 : kids.size();
    }
}
