package ru.gildor.hackernews.android.core.bindings;

import android.databinding.BindingAdapter;
import android.support.annotation.Nullable;
import android.text.method.MovementMethod;
import android.view.View;
import android.widget.TextView;
import ru.gildor.hackernews.android.core.providers.MovementMethodProvider;
import ru.gildor.hackernews.android.core.providers.TextFormatter;
import ru.gildor.hackernews.android.core.scopes.AppScope;

import javax.inject.Inject;

/**
 * Generic binding adapters that can be used in all layouts and not related to any activity
 */
@AppScope
public class CoreBindingAdapters {
    private TextFormatter textFormatter;
    private MovementMethod movementMethod;

    @Inject
    CoreBindingAdapters(TextFormatter textFormatter, MovementMethodProvider movementMethod) {
        this.textFormatter = textFormatter;
        this.movementMethod = movementMethod.provide();
    }

    @BindingAdapter("html")
    public void setHtml(TextView view, String text) {
        view.setText(textFormatter.fromHtml(text));
        view.setMovementMethod(movementMethod);
    }

    @BindingAdapter("visible")
    public void setVisibility(View view, @Nullable Boolean visible) {
        if (visible == null || !visible) {
            view.setVisibility(View.GONE);
        } else {
            view.setVisibility(View.VISIBLE);
        }
    }

}
