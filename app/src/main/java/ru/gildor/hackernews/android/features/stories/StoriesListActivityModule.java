package ru.gildor.hackernews.android.features.stories;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import dagger.Module;
import dagger.Provides;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.gildor.hackernews.android.R;
import ru.gildor.hackernews.android.core.rx.StateConsumer;
import ru.gildor.hackernews.android.core.scopes.ActivityScope;
import ru.gildor.hackernews.android.core.scopes.InitializedBinding;
import ru.gildor.hackernews.android.core.state.StateRepository;
import ru.gildor.hackernews.android.core.state.StateRepositoryImpl;
import ru.gildor.hackernews.android.databinding.ActivityStoriesListBinding;
import ru.gildor.hackernews.api.HackerNewsApi;
import ru.gildor.hackernews.api.models.Story;

import java.util.List;

@Module
public class StoriesListActivityModule {

    static final String STORIES_STATE = "stories_state";

    private static final int STORIES_PER_PAGE_LIMIT = 30;

    @Provides
    Activity providesActivity(StoriesListActivity activity) {
        return activity;
    }

    @Provides
    Observable<List<StoryViewModel>> provideStoriesListObservable(
            final HackerNewsApi api, StoriesListActivity activity,
            final StoryListTransformer transformer,
            final StateRepository<List<Story>> stateRepository
    ) {
        return Observable
                .concat(
                        stateRepository.consumeState().toObservable(),
                        api.loadTopStories(STORIES_PER_PAGE_LIMIT)
                                .doOnNext(new StateConsumer<>(stateRepository))
                )
                .subscribeOn(Schedulers.io())
                .firstOrError()
                .toObservable()
                .compose(activity.<List<Story>>bindToLifecycle())
                .map(transformer)
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Provides
    @ActivityScope
    StateRepository<List<Story>> provideStateRepository() {
        return new StateRepositoryImpl<>(STORIES_STATE);
    }

    @Provides
    @ActivityScope
    ActivityStoriesListBinding provideStoriesListBinding(StoriesListActivity activity) {
        return DataBindingUtil.setContentView(
                activity,
                R.layout.activity_stories_list
        );
    }

    @Provides
    @ActivityScope
    @InitializedBinding
    ActivityStoriesListBinding provideInitializedStoriesListBinding(
            ActivityStoriesListBinding binding,
            StoriesListViewModel model
    ) {
        binding.setModel(model);
        return binding;
    }

}