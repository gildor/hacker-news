package ru.gildor.hackernews.android.core.providers;

public interface Toaster {
    void showToast(String message);
}
