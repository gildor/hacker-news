package ru.gildor.hackernews.android.core.providers.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.format.DateUtils;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import ru.gildor.hackernews.android.core.providers.DateFormatter;

/**
 * {@link DateFormatter} implementation based on Android {@link DateUtils} class
 */
public final class DateUtilsDateFormatter implements DateFormatter {
    private Context context;

    @Inject
    DateUtilsDateFormatter(Context context) {
        this.context = context.getApplicationContext();
    }

    /**
     * Returns human readable relative date.
     *
     * @param timestampSeconds UNIX timestamp in seconds
     * @return human readable relative date
     */
    @NonNull
    @Override
    public String getRelativeDate(long timestampSeconds) {
        return DateUtils.getRelativeTimeSpanString(
                TimeUnit.SECONDS.toMillis(timestampSeconds)
        ).toString();
    }
}
