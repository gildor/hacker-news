package ru.gildor.hackernews.android.core.providers;

import io.reactivex.annotations.NonNull;
import ru.gildor.hackernews.api.models.Story;

public interface NavigationProvider {
    void openComments(@NonNull Story story);

    void navigateUp();

    void openUrl(String url);
}
