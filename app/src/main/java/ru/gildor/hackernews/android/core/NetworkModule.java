package ru.gildor.hackernews.android.core;

import dagger.Module;
import dagger.Provides;
import ru.gildor.hackernews.android.core.scopes.AppScope;
import ru.gildor.hackernews.api.DaggerHackerNewsApiComponent;
import ru.gildor.hackernews.api.HackerNewsApi;

/**
 * Module for network components.
 * Better to avoid use here any components related to Application,
 * better provide configuration objects instead
 */
@Module
public class NetworkModule {
    @Provides
    @AppScope
    HackerNewsApi provideHackerNewsApi() {
        return DaggerHackerNewsApiComponent.create().api();
    }
}
