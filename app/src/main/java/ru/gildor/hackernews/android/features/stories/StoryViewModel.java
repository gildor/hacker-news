package ru.gildor.hackernews.android.features.stories;

public interface StoryViewModel {
    int order();

    CharSequence title();

    String info();

    void openComments();

    void openUrl();

    int commentsCount();
}
