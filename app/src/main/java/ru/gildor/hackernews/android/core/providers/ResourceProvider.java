package ru.gildor.hackernews.android.core.providers;

import android.support.annotation.NonNull;
import android.support.annotation.PluralsRes;
import android.support.annotation.StringRes;

public interface ResourceProvider {
    @NonNull
    String getString(@StringRes int id);

    @NonNull
    String getString(@StringRes int id, Object... formatArgs);

    @NonNull
    String getQuantityString(@PluralsRes int id, int quantity, Object... formatArgs);
}
