package ru.gildor.hackernews.android.core.providers;

public interface TextFormatter {

    CharSequence formatUrl(String url);

    CharSequence concatSpans(CharSequence... text);

    CharSequence fromHtml(String text);
}
