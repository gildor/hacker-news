package ru.gildor.hackernews.android.core.state;

import android.os.Bundle;
import android.support.annotation.Nullable;
import io.reactivex.Maybe;
import io.reactivex.MaybeEmitter;
import io.reactivex.MaybeOnSubscribe;
import ru.gildor.hackernews.android.core.scopes.ActivityScope;
import timber.log.Timber;

import javax.inject.Inject;
import java.io.Serializable;

@ActivityScope
public class StateRepositoryImpl<T> implements StateRepository<T> {
    private String stateKey;
    @Nullable
    private T state = null;
    private boolean stateConsumed = false;

    @Inject
    public StateRepositoryImpl(String stateArg) {
        this.stateKey = stateArg;
    }

    @Override
    public void updateState(@Nullable T newState) {
        state = newState;
    }

    @Override
    public void saveState(@Nullable Bundle bundle) {
        if (state == null || bundle == null) {
            Timber.d("State is empty. Nothing to do");
        } else if (state instanceof Serializable) {
            bundle.putSerializable(stateKey, (Serializable) state);
        } else {
            throw new IllegalStateException("State " + state + " must implement Parcelable or Serializable");
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void restoreState(@Nullable Bundle bundle) {
        stateConsumed = false;
        state = bundle == null ? null : (T) bundle.getSerializable(stateKey);
    }

    @Override
    public Maybe<T> consumeState() {
        return Maybe.create(new MaybeOnSubscribe<T>() {
            @Override
            public void subscribe(MaybeEmitter<T> e) throws Exception {
                if (!stateConsumed && state != null) {
                    e.onSuccess(state);
                }
                e.onComplete();
                stateConsumed = true;
            }
        });
    }
}
