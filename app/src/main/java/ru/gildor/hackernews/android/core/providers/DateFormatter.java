package ru.gildor.hackernews.android.core.providers;

import android.support.annotation.NonNull;

public interface DateFormatter {

    @NonNull
    String getRelativeDate(long timestampSeconds);
}
