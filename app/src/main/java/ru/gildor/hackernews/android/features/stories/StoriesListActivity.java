package ru.gildor.hackernews.android.features.stories;

import android.os.Bundle;

import java.util.List;

import javax.inject.Inject;

import ru.gildor.hackernews.android.core.activities.BaseActivity;
import ru.gildor.hackernews.android.core.scopes.InitializedBinding;
import ru.gildor.hackernews.android.core.state.StateRepository;
import ru.gildor.hackernews.android.databinding.ActivityStoriesListBinding;
import ru.gildor.hackernews.api.models.Story;

public class StoriesListActivity extends BaseActivity {
    @Inject
    @InitializedBinding
    ActivityStoriesListBinding binding;

    @Inject
    StateRepository<List<Story>> stateRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        stateRepository.restoreState(savedInstanceState);
        //Start initial loading
        binding.getModel().refresh();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        stateRepository.saveState(outState);
    }
}
