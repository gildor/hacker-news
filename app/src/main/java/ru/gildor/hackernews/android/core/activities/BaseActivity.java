package ru.gildor.hackernews.android.core.activities;

import android.os.Bundle;

import com.trello.rxlifecycle2.components.support.RxAppCompatActivity;

import dagger.android.AndroidInjection;

public abstract class BaseActivity extends RxAppCompatActivity {
    protected void onCreate(Bundle savedInstanceState) {
        // Inject dependencies using Dagger Android
        // https://google.github.io/dagger//android.html
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
    }
}
