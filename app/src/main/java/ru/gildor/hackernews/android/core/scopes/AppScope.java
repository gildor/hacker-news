package ru.gildor.hackernews.android.core.scopes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Dagger scope annotation to mark dependencies coupled with Application lifecycle
 * In case of Android behavior of this scope is the same as @Singleton scope,
 * but we created it to be more explicit
 * <p>
 * See https://google.github.io/dagger/users-guide.html#singletons-and-scoped-bindings
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface AppScope {
}