package ru.gildor.hackernews.android.core.adapters;

import android.databinding.ViewDataBinding;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;

import java.util.Collections;
import java.util.List;

/**
 * RecyclerView adapter that uses static List to keep items data and DataBinding to render view
 *
 * @param <T> type of data binding model
 * @param <V> type of generated data binding that used to render RecyclerView items
 *            this data binding layout must have only one data variable with name "model"
 */
public class ListBindingAdapter<T, V extends ViewDataBinding> extends BindingAdapter<T, V> {
    private final List<T> items;

    public ListBindingAdapter(@Nullable List<T> items, @LayoutRes int layout) {
        super(layout);
        this.items = items == null ? Collections.<T>emptyList() : items;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    protected T getItem(int i) {
        return items.get(i);
    }


}
