package ru.gildor.hackernews.android.features.comments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import ru.gildor.hackernews.android.core.activities.BaseActivity;
import ru.gildor.hackernews.android.core.scopes.InitializedBinding;
import ru.gildor.hackernews.android.core.state.StateRepository;
import ru.gildor.hackernews.android.databinding.ActivityCommentsListBinding;
import ru.gildor.hackernews.api.models.Comment;
import ru.gildor.hackernews.api.models.Story;

import javax.inject.Inject;
import java.util.List;

import static ru.gildor.hackernews.android.features.comments.CommentsListActivityModule.COMMENT_ARG;

public class CommentsListActivity extends BaseActivity {

    @Inject
    @InitializedBinding
    ActivityCommentsListBinding binding;

    @Inject
    StateRepository<List<Comment>> stateRepository;

    public static Intent createIntent(Context context, Story story) {
        return new Intent(context, CommentsListActivity.class)
                .putExtra(COMMENT_ARG, story);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        stateRepository.restoreState(savedInstanceState);
        //Start loading
        binding.getModel().refresh();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        stateRepository.saveState(outState);
    }
}
