package ru.gildor.hackernews.android.core.providers.impl;

import android.content.Context;
import android.widget.Toast;
import ru.gildor.hackernews.android.core.providers.Toaster;

import javax.inject.Inject;

public class ToasterImpl implements Toaster {
    private Context context;

    @Inject
    ToasterImpl(Context context) {
        this.context = context;
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }
}
