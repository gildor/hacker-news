package ru.gildor.hackernews.android.features.comments;

import javax.inject.Inject;

import ru.gildor.hackernews.android.core.providers.DateFormatter;
import ru.gildor.hackernews.android.core.rx.ListTransformer;
import ru.gildor.hackernews.android.core.scopes.ActivityScope;
import ru.gildor.hackernews.api.models.Comment;

@ActivityScope
class CommentsListTransformer extends ListTransformer<Comment, CommentViewModel> {
    private DateFormatter dateFormatter;

    @Inject
    CommentsListTransformer(DateFormatter dateFormatter) {
        super();
        this.dateFormatter = dateFormatter;
    }

    @Override
    protected CommentViewModel transform(int index, Comment item) {
        return new CommentViewModelImpl(
                item,
                dateFormatter
        );
    }
}