package ru.gildor.hackernews.android.core.providers.impl;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.TypedValue;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import ru.gildor.hackernews.android.R;
import ru.gildor.hackernews.android.core.providers.TextFormatter;

import static android.text.Spanned.SPAN_EXCLUSIVE_EXCLUSIVE;

public class SpanTextFormatter implements TextFormatter {
    private ForegroundColorSpan urlColorSpan;
    private RelativeSizeSpan urlTextSizeSpan;

    @Inject
    SpanTextFormatter(@NonNull Context context) {
        urlColorSpan = new ForegroundColorSpan(
                ContextCompat.getColor(context, R.color.secondary_text)
        );
        TypedValue textSize = new TypedValue();
        context.getResources().getValue(R.dimen.url_text_size_relative, textSize, false);
        urlTextSizeSpan = new RelativeSizeSpan(textSize.getFloat());
    }

    @Override
    public CharSequence formatUrl(@NonNull String url) {
        SpannableString spannable = new SpannableString(url);
        int spanStart = 0;
        int spanEnd = url.length();
        spannable.setSpan(urlColorSpan, spanStart, spanEnd, SPAN_EXCLUSIVE_EXCLUSIVE);
        spannable.setSpan(urlTextSizeSpan, spanStart, spanEnd, SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannable;
    }

    @Override
    public CharSequence concatSpans(CharSequence... text) {
        return TextUtils.concat(text);
    }

    @SuppressWarnings("deprecation")
    @Override
    public CharSequence fromHtml(@NonNull String text) {
        // Replace paragraphs with new line for better formatting
        // There are no closing paragraph tags in Hacker News comments
        // Also trim to remove comment end new lines
        text = text.replace("<p>", "\n").trim();
        return Html.fromHtml(text);
    }
}
