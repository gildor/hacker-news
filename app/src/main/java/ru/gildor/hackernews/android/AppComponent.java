package ru.gildor.hackernews.android;

import android.databinding.DataBindingComponent;

import dagger.Component;
import dagger.android.AndroidInjectionModule;
import dagger.android.AndroidInjector;
import ru.gildor.hackernews.android.core.AppModule;
import ru.gildor.hackernews.android.core.NetworkModule;
import ru.gildor.hackernews.android.core.scopes.AppScope;

/**
 * Main Application component with support of Activity injection
 */
@AppScope
@Component(modules = {
        AppModule.class,
        NetworkModule.class,
        ActivityBindingModule.class,
        AndroidInjectionModule.class
})
public interface AppComponent extends AndroidInjector<App>, DataBindingComponent {
    @Component.Builder
    abstract class Builder extends AndroidInjector.Builder<App> {
        // Here you can add builders to override AppComponent modules.
        // For example:
        // abstract Builder appModule(AppModule appModule);

    }
}

