package ru.gildor.hackernews.android.core.providers.impl;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.content.ContextCompat;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import ru.gildor.hackernews.android.R;
import ru.gildor.hackernews.android.core.providers.NavigationProvider;
import ru.gildor.hackernews.android.core.scopes.ActivityScope;
import ru.gildor.hackernews.android.features.comments.CommentsListActivity;
import ru.gildor.hackernews.api.models.Story;

@ActivityScope
public final class ActivityNavigationProvider implements NavigationProvider {

    private final Activity activity;

    @Inject
    ActivityNavigationProvider(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void openComments(@NonNull Story story) {
        activity.startActivity(CommentsListActivity.createIntent(activity, story));
    }

    @Override
    public void navigateUp() {
        activity.onBackPressed();
    }

    @Override
    public void openUrl(String url) {
        Uri uri = Uri.parse(url);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
            CustomTabsIntent customTabs = new CustomTabsIntent.Builder()
                    .setToolbarColor(ContextCompat.getColor(activity, R.color.colorPrimary))
                    .setShowTitle(true)
                    .build();
            customTabs.launchUrl(activity, uri);
        } else {
            activity.startActivity(new Intent(Intent.ACTION_VIEW).setData(uri));
        }

    }
}
