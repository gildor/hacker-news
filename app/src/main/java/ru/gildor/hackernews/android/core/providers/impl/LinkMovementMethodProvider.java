package ru.gildor.hackernews.android.core.providers.impl;

import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import ru.gildor.hackernews.android.core.providers.MovementMethodProvider;
import ru.gildor.hackernews.android.core.scopes.AppScope;

import javax.inject.Inject;

@AppScope
public class LinkMovementMethodProvider implements MovementMethodProvider {
    @Inject
    LinkMovementMethodProvider() {
        super();
    }

    @Override
    public MovementMethod provide() {
        return LinkMovementMethod.getInstance();
    }
}
