package ru.gildor.hackernews.android.features.stories;

import javax.inject.Inject;

import ru.gildor.hackernews.android.core.providers.DateFormatter;
import ru.gildor.hackernews.android.core.providers.NavigationProvider;
import ru.gildor.hackernews.android.core.providers.ResourceProvider;
import ru.gildor.hackernews.android.core.providers.TextFormatter;
import ru.gildor.hackernews.android.core.rx.ListTransformer;
import ru.gildor.hackernews.android.core.scopes.ActivityScope;
import ru.gildor.hackernews.api.models.Story;

@ActivityScope
class StoryListTransformer extends ListTransformer<Story, StoryViewModel> {
    private final ResourceProvider res;
    private final NavigationProvider nav;
    private final TextFormatter formatter;
    private final DateFormatter date;

    @Inject
    StoryListTransformer(
            final ResourceProvider res,
            final NavigationProvider nav,
            final TextFormatter formatter,
            final DateFormatter date
    ) {
        super();
        this.res = res;
        this.nav = nav;
        this.formatter = formatter;
        this.date = date;
    }

    @Override
    protected StoryViewModel transform(int index, Story item) {
        return new StoryViewModelImpl(
                index + 1,
                item, res, nav, formatter, date
        );
    }
}