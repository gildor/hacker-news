package ru.gildor.hackernews.android.core;

import dagger.Binds;
import dagger.Module;
import ru.gildor.hackernews.android.core.providers.NavigationProvider;
import ru.gildor.hackernews.android.core.providers.impl.ActivityNavigationProvider;
import ru.gildor.hackernews.android.core.scopes.ActivityScope;

/**
 * Module with generic Activity dependencies not related to a particular activity
 */
@Module
public interface ActivityModule {
    @ActivityScope
    @Binds
    NavigationProvider provideNavigationProvider(ActivityNavigationProvider navigationProvider);
}
