package ru.gildor.hackernews.android.core.bindings;

import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import io.reactivex.Observable;
import ru.gildor.hackernews.android.core.providers.Toaster;

import java.util.List;

/**
 * Abstract view model for screens with toolbar and list of items
 *
 * @param <T> type of list item
 */
public abstract class SimpleListViewModel<T> implements ListViewModel<T> {
    private final Observable<List<T>> listObservable;
    protected ObservableField<List<T>> list = new ObservableField<>();
    protected ObservableField<String> error = new ObservableField<>();
    private ObservableBoolean isLoading = new ObservableBoolean(false);

    public SimpleListViewModel(Observable<List<T>> listObservable, final Toaster toaster) {
        this.listObservable = listObservable;
        error.addOnPropertyChangedCallback(new ErrorPropertyChangedCallback<>(toaster, error, list));
    }

    @Override
    public abstract String title();

    @Override
    public final ObservableBoolean isLoading() {
        return isLoading;
    }

    @Override
    public final ObservableField<List<T>> list() {
        return list;
    }

    @Override
    public ObservableField<String> error() {
        return error;
    }

    @Override
    public final void refresh() {
        listObservable.subscribe(new ListObserver<>(list, isLoading, error));
    }
}
