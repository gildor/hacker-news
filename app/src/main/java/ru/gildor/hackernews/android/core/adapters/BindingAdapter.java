package ru.gildor.hackernews.android.core.adapters;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import ru.gildor.hackernews.android.BR;

/**
 * Adapter for RecyclerView that can be used as universal adapter
 * for any RecyclerView that uses data bindings to render list items
 * <p>
 * Client must extend this class and implement method getItem() that should return model for data binding
 *
 * @param <T> type of data binding model
 * @param <V> type of generated data binding that used to render RecyclerView items
 *            this data binding layout must have only one data variable with name "model"
 */
public abstract class BindingAdapter<T, V extends ViewDataBinding> extends RecyclerView.Adapter<BindingViewHolder<V>> {
    private int layout;
    private LayoutInflater inflater = null;

    BindingAdapter(@LayoutRes int layout) {
        super();
        this.layout = layout;
    }

    @Override
    public BindingViewHolder<V> onCreateViewHolder(ViewGroup viewGroup, int i) {
        if (inflater == null) {
            inflater = LayoutInflater.from(viewGroup.getContext());
        }
        return new BindingViewHolder<>(
                //Inflate binding
                DataBindingUtil.<V>inflate(inflater, layout, viewGroup, false)
        );
    }

    @Override
    public void onBindViewHolder(BindingViewHolder<V> holder, int i) {
        holder.getBinding().setVariable(BR.model, getItem(i));
    }

    abstract protected T getItem(int i);

}
