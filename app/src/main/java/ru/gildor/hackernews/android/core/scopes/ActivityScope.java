package ru.gildor.hackernews.android.core.scopes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Dagger scope annotation to mark dependencies coupled with Activity lifecycle
 * See https://google.github.io/dagger/users-guide.html#releasable-references
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface ActivityScope {
}