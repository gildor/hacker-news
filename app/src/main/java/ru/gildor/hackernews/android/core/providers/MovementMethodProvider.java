package ru.gildor.hackernews.android.core.providers;

import android.text.method.MovementMethod;

public interface MovementMethodProvider {
    MovementMethod provide();
}
