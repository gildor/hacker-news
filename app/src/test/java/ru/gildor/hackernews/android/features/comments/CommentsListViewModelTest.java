package ru.gildor.hackernews.android.features.comments;

import io.reactivex.Observable;
import org.junit.Before;
import org.junit.Test;
import ru.gildor.hackernews.android.core.providers.NavigationProvider;
import ru.gildor.hackernews.android.core.providers.Toaster;
import ru.gildor.hackernews.android.features.stories.StubStory;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class CommentsListViewModelTest {
    private CommentsListViewModel model;
    private List<CommentViewModel> mockedList;
    private StubStory story;
    private NavigationProvider nav;
    private Toaster toaster;

    @SuppressWarnings("unchecked")
    @Before
    public void setUp() throws Exception {
        nav = mock(NavigationProvider.class);
        toaster = mock(Toaster.class);
        mockedList = Collections.singletonList(mock(CommentViewModel.class));
        story = new StubStory();
        model = new CommentsListViewModel(
                story,
                Observable.fromIterable(Collections.singletonList(mockedList)),
                nav,
                toaster
        );
    }

    @Test
    public void title() throws Exception {
        assertEquals(story.title(), model.title());
    }

    @Test
    public void onNavigationClick() throws Exception {
        model.onNavigationClick();
        verify(nav).navigateUp();
    }

    @Test
    public void isLoadingNonNull() {
        assertNotNull(model.isLoading());
    }

    @Test
    public void listNotNull() {
        assertNotNull(model.list());
    }

    @Test
    public void refresh() {
        assertNull(model.list().get());
        model.refresh();
        assertEquals(mockedList, model.list().get());
    }
}