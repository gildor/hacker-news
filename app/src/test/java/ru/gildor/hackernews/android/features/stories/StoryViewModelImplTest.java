package ru.gildor.hackernews.android.features.stories;

import org.junit.Before;
import org.junit.Test;
import ru.gildor.hackernews.android.R;
import ru.gildor.hackernews.android.core.providers.DateFormatter;
import ru.gildor.hackernews.android.core.providers.NavigationProvider;
import ru.gildor.hackernews.android.core.providers.ResourceProvider;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static ru.gildor.hackernews.android.features.stories.StubTextFormatter.FORMATTED_URL;

public class StoryViewModelImplTest {

    private static final int DEFAULT_ORDER = 5;
    private static final String RELATIVE_DATE = "1 day ago";
    private StoryViewModel model;
    private NavigationProvider nav;
    private StubStory story;
    private ResourceProvider res;

    @Before
    public void setUp() throws Exception {
        initModel(new StubStory());
    }

    private void initModel(StubStory story) {
        this.story = story;
        nav = mock(NavigationProvider.class);
        res = mock(ResourceProvider.class);
        DateFormatter date = mock(DateFormatter.class);
        when(date.getRelativeDate(story.time())).thenReturn(RELATIVE_DATE);
        model = new StoryViewModelImpl(
                DEFAULT_ORDER,
                story,
                res,
                nav,
                new StubTextFormatter(),
                date
        );
    }

    @Test
    public void order() throws Exception {
        assertEquals(DEFAULT_ORDER, model.order());
    }

    @Test
    public void title() throws Exception {
        assertEquals(
                Arrays.toString(new String[]{story.title(), " ", FORMATTED_URL}),
                model.title()
        );
    }

    @Test
    public void titleWithEmptyUrl() throws Exception {
        initModel(new StubStory() {
            @Override
            public String url() {
                return null;
            }
        });
        assertEquals(
                Arrays.toString(new String[]{story.title(), " ", ""}),
                model.title()
        );
    }

    @Test
    public void emptyUrl() throws Exception {
        assertEquals(
                Arrays.toString(new String[]{story.title(), " ", FORMATTED_URL}),
                model.title()
        );
    }

    @Test
    public void info() throws Exception {
        String quantity = "42 comments";
        when(res.getQuantityString(R.plurals.points, story.score(), story.score())).thenReturn(quantity);
        model.info();
        verify(res).getString(
                R.string.story_info,
                quantity,
                story.by(),
                RELATIVE_DATE
        );
    }

    @Test
    public void openComments() throws Exception {
        model.openComments();
        verify(nav).openComments(story);
    }

    @Test
    public void openUrl() throws Exception {
        model.openUrl();
        verify(nav).openUrl(story.url());
    }

    @Test
    public void commentsCount() throws Exception {
        assertEquals(story.kids().size(), model.commentsCount());
    }

    @Test
    public void emptyComments() throws Exception {
        initModel(new StubStory() {
            @Override
            public List<Long> kids() {
                return null;
            }
        });
        assertEquals(0, model.commentsCount());
    }
}