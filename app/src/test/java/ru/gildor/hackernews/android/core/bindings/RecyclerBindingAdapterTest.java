package ru.gildor.hackernews.android.core.bindings;

import android.support.v7.widget.RecyclerView;

import org.junit.Test;
import org.mockito.ArgumentMatcher;

import java.util.Arrays;

import ru.gildor.hackernews.android.R;
import ru.gildor.hackernews.android.core.adapters.ListBindingAdapter;

import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class RecyclerBindingAdapterTest {
    @Test
    public void setStoryListAdapter() throws Exception {
        RecyclerView mock = mock(RecyclerView.class);
        new RecyclerBindingAdapter().setListAdapter(
                mock,
                R.layout.item_comment,
                Arrays.asList(1, 2, 3, 4, 5)
        );

        verify(mock).setAdapter(argThat(new ArgumentMatcher<RecyclerView.Adapter>() {
            @Override
            public boolean matches(RecyclerView.Adapter adapter) {
                return adapter instanceof ListBindingAdapter &&
                        adapter.getItemCount() == 5;
            }
        }));
    }

}