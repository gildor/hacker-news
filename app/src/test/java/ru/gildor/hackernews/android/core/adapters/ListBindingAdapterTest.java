package ru.gildor.hackernews.android.core.adapters;

import android.databinding.ViewDataBinding;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import ru.gildor.hackernews.android.R;

import static org.junit.Assert.assertEquals;

public class ListBindingAdapterTest {

    private List<Integer> list;
    private ListBindingAdapter<Integer, ViewDataBinding> adapter;

    @Before
    public void setUp() throws Exception {
        list = Arrays.asList(1, 2, 3, 4, 5);
        adapter = new ListBindingAdapter<>(list, R.layout.item_comment);
    }

    @Test
    public void emptyList() throws Exception {
        ListBindingAdapter<Object, ViewDataBinding> emptyAdapter = new ListBindingAdapter<>(
                Collections.emptyList(),
                R.layout.item_comment
        );
        assertEquals(0, emptyAdapter.getItemCount());
    }

    @Test
    public void nullList() throws Exception {
        ListBindingAdapter<Object, ViewDataBinding> emptyAdapter = new ListBindingAdapter<>(
                null,
                R.layout.item_comment
        );
        assertEquals(0, emptyAdapter.getItemCount());
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void getItemOutOfBounds() throws Exception {
        adapter.getItem(100);
    }

    @Test
    public void getItemCount() throws Exception {
        assertEquals(list.size(), adapter.getItemCount());
    }

    @Test
    public void getItem() throws Exception {
        for (int i = 0; i < list.size(); i++) {
            assertEquals(list.get(i), adapter.getItem(i));
        }
    }

}