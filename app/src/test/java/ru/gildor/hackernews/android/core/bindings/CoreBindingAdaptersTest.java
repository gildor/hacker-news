package ru.gildor.hackernews.android.core.bindings;

import android.widget.TextView;
import org.junit.Test;
import ru.gildor.hackernews.android.core.providers.MovementMethodProvider;
import ru.gildor.hackernews.android.core.providers.TextFormatter;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class CoreBindingAdaptersTest {
    @Test
    public void setHtml() throws Exception {
        TextFormatter formatter = mock(TextFormatter.class);
        MovementMethodProvider movementMethod = mock(MovementMethodProvider.class);
        CharSequence output = "formatted output";
        when(formatter.fromHtml(anyString())).thenReturn(output);
        TextView view = mock(TextView.class);
        new CoreBindingAdapters(formatter, movementMethod).setHtml(view, "input");
        verify(view).setText(output);
    }

}