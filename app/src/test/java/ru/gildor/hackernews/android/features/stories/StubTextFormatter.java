package ru.gildor.hackernews.android.features.stories;

import java.util.Arrays;

import ru.gildor.hackernews.android.core.providers.TextFormatter;

public class StubTextFormatter implements TextFormatter {
    static final String FORMATTED_URL = "formatted://url";
    private static final String HTML_TEXT = "html text";

    @Override
    public CharSequence formatUrl(String url) {
        return FORMATTED_URL;
    }

    @Override
    public CharSequence concatSpans(CharSequence... text) {
        return Arrays.toString(text);
    }

    @Override
    public CharSequence fromHtml(String text) {
        return HTML_TEXT;
    }
}
