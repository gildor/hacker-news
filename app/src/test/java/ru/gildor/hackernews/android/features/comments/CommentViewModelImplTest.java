package ru.gildor.hackernews.android.features.comments;

import android.support.annotation.NonNull;

import org.junit.Before;
import org.junit.Test;

import ru.gildor.hackernews.android.core.providers.DateFormatter;
import ru.gildor.hackernews.android.features.stories.StubStory;

import static org.junit.Assert.assertEquals;

public class CommentViewModelImplTest {

    private CommentViewModelImpl model;
    private StubStory comment;

    @Before
    public void setUp() throws Exception {
        comment = new StubStory();
        model = new CommentViewModelImpl(comment, new StubDateFormatter());
    }

    @Test
    public void text() throws Exception {
        assertEquals(comment.text(), model.text());
    }

    @Test
    public void info() throws Exception {
        String expected = comment.by() + " " + comment.time() + " seconds ago";
        assertEquals(expected, model.info());
    }

    private static class StubDateFormatter implements DateFormatter {

        @NonNull
        @Override
        public String getRelativeDate(long timestampSeconds) {
            return timestampSeconds + " seconds ago";
        }
    }
}