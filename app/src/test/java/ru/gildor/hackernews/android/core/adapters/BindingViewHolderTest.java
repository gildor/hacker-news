package ru.gildor.hackernews.android.core.adapters;

import android.databinding.ViewDataBinding;
import android.view.View;

import org.junit.Assert;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class BindingViewHolderTest {
    @Test
    public void getBinding() throws Exception {
        ViewDataBinding binding = mock(ViewDataBinding.class);
        when(binding.getRoot()).thenReturn(mock(View.class));
        BindingViewHolder<ViewDataBinding> holder = new BindingViewHolder<>(binding);
        Assert.assertEquals(binding, holder.getBinding());
    }

}