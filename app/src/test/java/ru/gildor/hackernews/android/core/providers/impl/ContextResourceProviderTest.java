package ru.gildor.hackernews.android.core.providers.impl;

import android.content.Context;
import android.content.res.Resources;

import org.junit.Before;
import org.junit.Test;

import ru.gildor.hackernews.android.R;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ContextResourceProviderTest {
    private Context context;
    private ContextResourceProvider resourceProvider;
    private Resources resources;

    @Before
    public void setUp() throws Exception {
        resources = mock(Resources.class);
        context = mock(Context.class);
        //Use same mock for application context
        when(context.getApplicationContext()).thenReturn(context);
        when(context.getResources()).thenReturn(resources);
        resourceProvider = new ContextResourceProvider(context);
    }

    @Test
    public void getString() throws Exception {
        resourceProvider.getString(R.string.app_name);
        verify(context).getString(R.string.app_name);
    }

    @Test
    public void getStringWithArgs() throws Exception {
        resourceProvider.getString(R.string.story_info, "one", "2", "III");
        verify(context).getString(R.string.story_info, "one", "2", "III");
    }

    @Test
    public void getQuantityString() throws Exception {
        resourceProvider.getQuantityString(R.plurals.points, 42, "42");
        verify(resources).getQuantityString(R.plurals.points, 42, "42");
    }

}