package ru.gildor.hackernews.android.features.stories;

import org.junit.Test;

import ru.gildor.hackernews.android.core.providers.DateFormatter;
import ru.gildor.hackernews.android.core.providers.NavigationProvider;
import ru.gildor.hackernews.android.core.providers.ResourceProvider;
import ru.gildor.hackernews.android.core.providers.TextFormatter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;

public class StoryListTransformerTest {
    @Test
    public void transform() throws Exception {
        StoryListTransformer transformer = new StoryListTransformer(
                mock(ResourceProvider.class),
                mock(NavigationProvider.class),
                mock(TextFormatter.class),
                mock(DateFormatter.class)
        );

        StubStory story = new StubStory();
        StoryViewModel model = transformer.transform(41, story);
        assertNotNull(model);
        // first item number is 1 for user instead index 0
        assertEquals(42, model.order());
    }

}