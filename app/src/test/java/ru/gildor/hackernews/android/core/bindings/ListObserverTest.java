package ru.gildor.hackernews.android.core.bindings;

import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import io.reactivex.disposables.Disposable;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static junit.framework.Assert.*;
import static org.junit.Assert.assertEquals;

public class ListObserverTest {

    private ObservableField<List<String>> list;
    private ObservableField<String> error;
    private ObservableBoolean isLoading;
    private ListObserver<String> observer;

    @Before
    public void setUp() throws Exception {
        list = new ObservableField<>();
        isLoading = new ObservableBoolean();
        error = new ObservableField<>();
        observer = new ListObserver<>(list, isLoading, error);
    }

    @Test
    public void onSubscribe() throws Exception {
        assertFalse(isLoading.get());
        observer.onSubscribe(Mockito.mock(Disposable.class));
        assertTrue(isLoading.get());
    }

    @Test
    public void onNext() throws Exception {
        assertNull(list.get());
        List<String> empty = Collections.emptyList();
        observer.onNext(empty);
        junit.framework.Assert.assertEquals(empty, list.get());
        List<String> abc = Arrays.asList("a", "b", "c");
        observer.onNext(abc);
        junit.framework.Assert.assertEquals(abc, list.get());
    }

    @Test
    public void onError() throws Exception {
        observer.onSubscribe(Mockito.mock(Disposable.class));
        assertNull(error.get());
        assertTrue(isLoading.get());
        observer.onError(new IllegalStateException("List error"));
        assertFalse(isLoading.get());
        assertEquals("List error", error.get());
    }

    @Test
    public void onComplete() throws Exception {
        observer.onSubscribe(Mockito.mock(Disposable.class));
        assertTrue(isLoading.get());
        observer.onComplete();
        assertFalse(isLoading.get());
    }

}