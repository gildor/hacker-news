package ru.gildor.hackernews.android.features.stories;

import io.reactivex.Observable;
import org.junit.Before;
import org.junit.Test;
import ru.gildor.hackernews.android.R;
import ru.gildor.hackernews.android.core.providers.ResourceProvider;
import ru.gildor.hackernews.android.core.providers.Toaster;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class StoriesListViewModelTest {
    private StoriesListViewModel model;
    private ResourceProvider res;
    private List<StoryViewModel> mockedList;
    private Toaster toaster;

    @SuppressWarnings("unchecked")
    @Before
    public void setUp() throws Exception {
        res = mock(ResourceProvider.class);
        mockedList = Collections.singletonList(mock(StoryViewModel.class));
        toaster = mock(Toaster.class);
        model = new StoriesListViewModel(
                Observable.fromIterable(Collections.singletonList(mockedList)),
                res,
                toaster
        );
    }

    @Test
    public void title() throws Exception {
        String name = "App name";
        when(res.getString(R.string.app_name)).thenReturn(name);
        assertEquals(name, model.title());
    }

    @Test
    public void isLoadingNonNull() {
        assertNotNull(model.isLoading());
    }

    @Test
    public void listNotNull() {
        assertNotNull(model.list());
    }

    @Test
    public void refresh() {
        assertNull(model.list().get());
        model.refresh();
        assertEquals(mockedList, model.list().get());
    }

}