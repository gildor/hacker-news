package ru.gildor.hackernews.android.features.comments;

import org.junit.Test;

import ru.gildor.hackernews.android.core.providers.DateFormatter;
import ru.gildor.hackernews.android.features.stories.StubStory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;

public class CommentsListTransformerTest {
    @Test
    public void transform() throws Exception {
        DateFormatter date = mock(DateFormatter.class);
        StubStory comment = new StubStory();
        CommentViewModel model = new CommentsListTransformer(date).transform(0, comment);
        assertNotNull(model);
        assertEquals(comment.text(), model.text());
    }
}