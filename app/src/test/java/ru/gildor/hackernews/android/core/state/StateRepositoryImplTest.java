package ru.gildor.hackernews.android.core.state;

import android.os.Bundle;
import org.junit.Before;
import org.junit.Test;
import ru.gildor.hackernews.android.features.comments.CommentViewModel;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class StateRepositoryImplTest {

    private static final String TEST_STATE = "test_state";
    private StateRepositoryImpl<String> state;
    private Bundle bundle;

    @Before
    public void setUp() throws Exception {
        state = new StateRepositoryImpl<>("test_state");
        bundle = mock(Bundle.class);
        when(bundle.getSerializable(TEST_STATE)).thenReturn("hi");
    }

    @Test
    public void updateState() throws Exception {
        state.updateState("state");
        state.updateState("state2");
        state.saveState(bundle);
        verify(bundle).putSerializable(TEST_STATE, "state2");
        assertEquals("state2", state.consumeState().blockingGet());
    }

    @Test
    public void nullBundle() throws Exception {
        state.saveState(null);
    }

    @Test
    public void saveState() throws Exception {
        state.updateState("state");
        assertEquals("state", state.consumeState().blockingGet());
    }

    @Test(expected = IllegalStateException.class)
    public void nonSerializableState() throws Exception {
        StateRepositoryImpl<CommentViewModel> wrongState = new StateRepositoryImpl<>("test");
        //Non serializable state
        wrongState.updateState(new CommentViewModel() {

            @Override
            public String text() {
                return null;
            }

            @Override
            public String info() {
                return null;
            }
        });
        wrongState.saveState(bundle);
    }

    @Test
    public void restoreState() throws Exception {
        state.restoreState(bundle);
        state.consumeState().test().assertValue("hi");
    }

    @Test
    public void restoreEmptyState() throws Exception {
        when(bundle.getSerializable(TEST_STATE)).thenReturn(null);
        state.restoreState(bundle);
        verify(bundle).getSerializable(TEST_STATE);
        state.consumeState().test().assertValueCount(0);
    }

    @Test
    public void consumeState() throws Exception {
        state.consumeState().test().assertValueCount(0);
        state.restoreState(bundle);
        state.consumeState().test().assertValue("hi");
        //State must be empty after consume
        state.consumeState().test().assertValueCount(0);
    }

}