package ru.gildor.hackernews.android.core.rx;

import org.junit.Test;
import ru.gildor.hackernews.android.core.state.StateRepository;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class StateConsumerTest {
    @SuppressWarnings("unchecked")
    @Test
    public void accept() throws Exception {
        StateRepository<String> repository = mock(StateRepository.class);
        StateConsumer<String> consumer = new StateConsumer<>(repository);
        consumer.accept("one");
        verify(repository).updateState("one");
        consumer.accept("two");
        verify(repository).updateState("two");
    }

}