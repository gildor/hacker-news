package ru.gildor.hackernews.android.core.bindings;

import android.databinding.ObservableField;
import org.junit.Before;
import org.junit.Test;
import ru.gildor.hackernews.android.core.providers.Toaster;

import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class ErrorPropertyChangedCallbackTest {

    private ObservableField<String> error;
    private ObservableField<List<String>> list;
    private Toaster toaster;

    @Before
    public void setUp() throws Exception {
        toaster = mock(Toaster.class);
        error = new ObservableField<>();
        list = new ObservableField<>();
        error.addOnPropertyChangedCallback(new ErrorPropertyChangedCallback<>(toaster, error, list));
    }

    @Test
    public void onPropertyChanged() throws Exception {
        list.set(Arrays.asList("one", "two"));
        verify(toaster, never()).showToast(anyString());
        error.set("error");
        verify(toaster).showToast("error");
    }

    @Test
    public void onPropertyChangedWithEmptyList() throws Exception {
        error.set("error");
        verify(toaster, never()).showToast(anyString());
        error.set(null);
        verify(toaster, never()).showToast(anyString());
        list.set(Arrays.asList("one", "two"));
        error.set("error2");
        verify(toaster).showToast("error2");
    }

}