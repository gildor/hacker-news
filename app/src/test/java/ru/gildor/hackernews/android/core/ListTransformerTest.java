package ru.gildor.hackernews.android.core;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import ru.gildor.hackernews.android.core.rx.ListTransformer;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;

public class ListTransformerTest {

    @Test
    public void listOfStringsToListOfInts() throws Exception {
        List<Integer> result = new StringToIntListTransformer()
                .apply(Arrays.asList("1", "2", "3"));
        assertArrayEquals("Transformed list is wrong",
                new Integer[]{1, 2, 3},
                result.toArray()
        );
    }

    @Test
    public void emptyList() throws Exception {
        List<Integer> result = new StringToIntListTransformer().apply(Collections.<String>emptyList());
        assertTrue(result.isEmpty());
    }

    @Test(expected = NullPointerException.class)
    public void passNullToApply() throws Exception {
        new StringToIntListTransformer().apply(null);
    }

    private class StringToIntListTransformer extends ListTransformer<String, Integer> {
        @Override
        protected Integer transform(int index, String item) {
            return Integer.parseInt(item);
        }
    }
}