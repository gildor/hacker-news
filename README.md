# Hacker News for Android
Application to read news and comments from [Y Combinator Hacker News](https://news.ycombinator.com/)

## Project structure
Hacker News application separated on two modules:

- app - Android application
- rest-api - pure Java Rest API implementation

### Module [app](app)
Android application written on pure Android SDK with min API level 11 and target 25

Main architecture ideas:
- Trying to keep all components separately and allow to move them to different module in future.
- Use DI everywhere where it's possible to help decouple components
- Most of code should be testable with Unit tests
- ViewModels shouldn't use any Android components inside, especially Views and Activities
- Use Instrumentation tests only when you cannot test component in Unit. But still use abstraction for that component.

We use:
- [Data Binding Library](https://developer.android.com/topic/libraries/data-binding/index.html) for UI and work with views
- [Dagger](https://github.com/google/dagger) for dependency injection
- [Dagger Android](https://google.github.io/dagger/android.html) for implicit inject of Android components
- Espresso for Instrumentation tests

#### Tests and Code Coverage
To run unit tests, instrumentation tests and code coverage reports just run:
```
./gradlew jacocoTestReport
```
Results:

[Unit reports](app/build/reports/tests/testDebugUnitTest/index.html)

[Instrumentation reports](app/build/reports/androidTests/connected/index.html)

[Coverage (unit + instrumentation) reports](app/build/reports/jacoco/jacocoTestDebugUnitTestReport/html/index.html)


### Module [rest-api](rest-api)
This is pure Java module and can be used not only with Android.
So you can easily create test module (Android or Java) to test API or to play with it.

Main architecture idea to keep very small public API for client (mobile, backend or testing)

That allows us easily replace any implementation details, modify data objects and even replace API implementation 

Uses:
- RxJava2 for all asynchronous code
- Dagger for dependency injection
- [AutoValue](https://github.com/google/auto/tree/master/value) for date class generation
- [Moshi](https://github.com/square/moshi) for Json mapping and serialization (with AutoValue adapter generation)

#### Tests and Code Coverage
To run tests and code coverage reports just run:
```
./gradlew :rest-api:check
```
Results:

[Test reports](rest-api/build/reports/tests/test/index.html)

[Coverage reports](rest-api/build/reports/jacoco/test/html/index.html)

## TODO
* [ ] DataBase and diffs for requests
* [ ] Comments nesting (now only first level comments)
* [ ] Deep Links
* [ ] CheckStyle/PMD/findbugs
* [ ] Move activities to separate modules
* [ ] JavaDoc generation
* [ ] Proguard
* [ ] [Java 8 lambdas](https://developer.android.com/studio/preview/features/java8-support.html)
* [ ] Kotlin!
* [ ] Coverage monitoring service
* [ ] Instrumentation tests on CI
* [x] CI
