package ru.gildor.hackernews.api.internal.rest;

import dagger.Module;
import dagger.Provides;
import okhttp3.logging.HttpLoggingInterceptor;
import ru.gildor.hackernews.api.logging.LogLevel;
import ru.gildor.hackernews.api.logging.Logger;

import javax.inject.Named;

@Module
public class LoggingModule {

    @Provides
    Logger provideLogger(@Named("DefaultHttp") final HttpLoggingInterceptor.Logger logger) {
        return new Logger() {
            @Override
            public void log(String message) {
                logger.log(message);
            }
        };
    }

    @Provides
    @Named("DefaultHttp")
    HttpLoggingInterceptor.Logger provideHttpLogger() {
        return HttpLoggingInterceptor.Logger.DEFAULT;
    }

    @Provides
    LogLevel provideLogLevel() {
        return LogLevel.BASIC;
    }
}
