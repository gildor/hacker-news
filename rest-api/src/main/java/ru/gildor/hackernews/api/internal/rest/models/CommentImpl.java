package ru.gildor.hackernews.api.internal.rest.models;

import com.google.auto.value.AutoValue;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import ru.gildor.hackernews.api.models.Comment;

import java.util.List;

@AutoValue
public abstract class CommentImpl implements Comment {
    public static JsonAdapter<CommentImpl> typeAdapter(Moshi moshi) {
        return new AutoValue_CommentImpl.MoshiJsonAdapter(moshi);
    }

    public static CommentImpl create(long id, String by, List<Long> kids,
                                     long time, Long parent, String text, boolean deleted) {
        return new AutoValue_CommentImpl(id, by, kids, time, Type.COMMENT, parent, text, deleted);
    }
}
