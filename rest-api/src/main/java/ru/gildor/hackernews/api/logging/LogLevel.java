package ru.gildor.hackernews.api.logging;

public enum LogLevel {
    NONE,
    BASIC,
    ALL
}
