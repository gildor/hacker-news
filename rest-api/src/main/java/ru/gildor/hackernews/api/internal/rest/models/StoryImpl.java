package ru.gildor.hackernews.api.internal.rest.models;

import com.google.auto.value.AutoValue;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import ru.gildor.hackernews.api.models.Story;

import java.util.List;

@AutoValue
abstract public class StoryImpl implements Story {
    public static JsonAdapter<StoryImpl> typeAdapter(Moshi moshi) {
        return new AutoValue_StoryImpl.MoshiJsonAdapter(moshi);
    }

    public static StoryImpl create(long id, String by, List<Long> kids,
                                   long time, Type type, String title,
                                   int descendants, int score, String url) {
        return new AutoValue_StoryImpl(id, by, kids, time, type, title, descendants, score, url);
    }
}
