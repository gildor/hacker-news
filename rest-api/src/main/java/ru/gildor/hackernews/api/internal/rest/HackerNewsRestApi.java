package ru.gildor.hackernews.api.internal.rest;

import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import ru.gildor.hackernews.api.HackerNewsApi;
import ru.gildor.hackernews.api.models.BaseStory;
import ru.gildor.hackernews.api.models.Comment;
import ru.gildor.hackernews.api.models.Story;

class HackerNewsRestApi implements HackerNewsApi {
    private final HackerNewsService service;

    HackerNewsRestApi(HackerNewsService service) {
        this.service = service;
    }

    @Override
    public Observable<List<Story>> loadTopStories(final int limit) {
        return service
                .getTopStories()
                .flatMapObservable(new Function<List<Integer>, ObservableSource<Story>>() {
                    @Override
                    public ObservableSource<Story> apply(List<Integer> stories) throws Exception {
                        return Observable.fromIterable(stories)
                                .take(limit)
                                // Use concatMapEager to load stories in parallel, but keep order
                                .concatMapEager(new Function<Integer, ObservableSource<Story>>() {
                                    @Override
                                    public ObservableSource<Story> apply(Integer storyId) throws Exception {
                                        return loadStory(storyId);
                                    }
                                });
                    }
                })
                //Top stories request can return not only stories, but also another types, like Job or Poll
                //But because we doesn't support other types rendering we must skip them
                .filter(new Predicate<Story>() {
                    @Override
                    public boolean test(Story story) throws Exception {
                        return story.type() == BaseStory.Type.STORY;
                    }
                })
                .toList()
                .toObservable();

    }

    @Override
    public Observable<List<Comment>> loadStoryComments(@NonNull Story story) {
        List<Long> kids = story.kids();
        return loadStoryComments(kids == null ? Collections.<Long>emptyList() : kids)
                .flatMap(new Function<List<Comment>, ObservableSource<List<Comment>>>() {
                    @Override
                    public ObservableSource<List<Comment>> apply(List<Comment> comments) throws Exception {
                        return Observable.fromIterable(comments).filter(new Predicate<Comment>() {
                            @Override
                            public boolean test(Comment comment) throws Exception {
                                return !comment.deleted();
                            }
                        }).toList().toObservable();
                    }
                });

    }

    @Override
    public Observable<List<Comment>> loadStoryComments(@NonNull List<Long> commentIds) {
        return Observable.fromIterable(commentIds)
                // Use concatMapEager to load comments in parallel, but keep order
                .concatMapEager(new Function<Long, ObservableSource<Comment>>() {

                    @Override
                    public ObservableSource<Comment> apply(Long commentId) throws Exception {
                        return loadComment(commentId);
                    }
                })
                .toList().toObservable();

    }

    @Override
    public Observable<Story> loadStory(long storyId) {
        return service.getStory(storyId).toObservable().cast(Story.class);
    }

    @Override
    public Observable<Comment> loadComment(long commentId) {
        return service.getComment(commentId).toObservable().cast(Comment.class);
    }

    @Override
    public Observable<List<Comment>> loadChildComments(Comment comment) {
        return Observable.fromIterable(comment.kids())
                .concatMapEager(new Function<Long, ObservableSource<Comment>>() {
                    @Override
                    public ObservableSource<Comment> apply(Long commentId) throws Exception {
                        return loadComment(commentId);
                    }
                })
                .toList()
                .toObservable();
    }
}
