package ru.gildor.hackernews.api.models;

import com.squareup.moshi.Json;

import java.io.Serializable;
import java.util.List;

import io.reactivex.annotations.Nullable;

/**
 * Base interface for Hacker News Story and Comment items.
 * Contains only common data between all story types
 */
public interface BaseStory extends Serializable {
    long id();

    @Nullable
    String by();
    @Nullable
    List<Long> kids();
    long time();
    Type type();

    enum Type {
        @Json(name = "story")
        STORY,
        @Json(name = "comment")
        COMMENT,
        @Json(name = "job")
        JOB,
        @Json(name = "poll")
        POLL,
        @Json(name = "pollopt")
        POLLOPT
    }
}
