package ru.gildor.hackernews.api;

import dagger.Component;
import ru.gildor.hackernews.api.internal.rest.RestApiModule;

@Component(modules = RestApiModule.class)
public interface HackerNewsApiComponent {
    HackerNewsApi api();
}

