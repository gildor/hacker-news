package ru.gildor.hackernews.api.models.autovalue;

import com.ryanharter.auto.value.moshi.MoshiAdapterFactory;
import com.squareup.moshi.JsonAdapter;

@MoshiAdapterFactory
public abstract class AutoTypeAdapterFactory implements JsonAdapter.Factory {
    // Static factory method to access the package
    // private generated implementation
    public static AutoTypeAdapterFactory create() {
        return new AutoValueMoshi_AutoTypeAdapterFactory();
    }
}
