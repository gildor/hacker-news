package ru.gildor.hackernews.api.internal.rest;


import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import ru.gildor.hackernews.api.HackerNewsApi;

@Module(includes = RetrofitModule.class)
public class RestApiModule {

    @Provides
    HackerNewsService provideHackerNewsService(Retrofit retrofit) {
        return retrofit.create(HackerNewsService.class);
    }

    @Provides
    HackerNewsApi provideHackerNewsApi(HackerNewsService service) {
        return new HackerNewsRestApi(service);
    }
}
