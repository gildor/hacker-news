package ru.gildor.hackernews.api.logging;

public interface Logger {
    void log(String message);
}

