package ru.gildor.hackernews.api.models;

import io.reactivex.annotations.Nullable;

/**
 * https://hacker-news.firebaseio.com/v0/item/14563602.json
 */
public interface Comment extends BaseStory {
    Long parent();

    @Nullable
    String text();

    boolean deleted();
}
