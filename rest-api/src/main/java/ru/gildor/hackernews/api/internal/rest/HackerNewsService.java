package ru.gildor.hackernews.api.internal.rest;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import ru.gildor.hackernews.api.internal.rest.models.CommentImpl;
import ru.gildor.hackernews.api.internal.rest.models.StoryImpl;

import java.util.List;

/**
 * Rest API for Hacker News
 * @see <a href="https://github.com/HackerNews/API">Hacker News API docs</a>
 */
public interface HackerNewsService {
    @GET("topstories.json")
    Single<List<Integer>> getTopStories();

    @GET("item/{id}.json")
    Single<StoryImpl> getStory(@Path("id") long storyId);

    @GET("item/{id}.json")
    Single<CommentImpl> getComment(@Path("id") long commentId);
}

