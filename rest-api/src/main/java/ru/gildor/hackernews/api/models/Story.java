package ru.gildor.hackernews.api.models;

import io.reactivex.annotations.Nullable;

/**
 *
 *
 * Example: https://hacker-news.firebaseio.com/v0/item/14563118.json
 */
public interface Story extends BaseStory {
    String title();
    int descendants();
    int score();

    @Nullable
    String url();

}
