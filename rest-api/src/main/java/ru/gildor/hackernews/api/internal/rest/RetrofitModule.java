package ru.gildor.hackernews.api.internal.rest;


import com.squareup.moshi.Moshi;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.CallAdapter;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.moshi.MoshiConverterFactory;
import ru.gildor.hackernews.api.logging.LogLevel;
import ru.gildor.hackernews.api.logging.Logger;
import ru.gildor.hackernews.api.models.autovalue.AutoTypeAdapterFactory;

import javax.inject.Named;

@Module(includes = LoggingModule.class)
public class RetrofitModule {

    static final String API_ENDPOINT = "https://hacker-news.firebaseio.com/v0/";

    @Provides
    @Named("baseApiUrl")
    String provideApiUrl() {
        return API_ENDPOINT;
    }

    @Provides
    Moshi provideMoshi() {
        return new Moshi.Builder()
                .add(AutoTypeAdapterFactory.create())
                .build();
    }

    @Provides
    HttpLoggingInterceptor.Level provideLogLevel(LogLevel logLevel) {
        if (logLevel == null) throw new NullPointerException("Log level is null");
        if (logLevel == LogLevel.ALL) {
            return HttpLoggingInterceptor.Level.BODY;
        } else if (logLevel == LogLevel.BASIC) {
            return HttpLoggingInterceptor.Level.BASIC;
        } else {
            return HttpLoggingInterceptor.Level.NONE;
        }
    }

    @Provides
    HttpLoggingInterceptor.Logger provideLogger(final Logger logger) {
        return new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {
                logger.log(message);
            }
        };
    }

    @Provides
    OkHttpClient provideOkHttpClient(final HttpLoggingInterceptor.Logger logger, HttpLoggingInterceptor.Level logLevel) {
        return new OkHttpClient.Builder()
                .addInterceptor(new HttpLoggingInterceptor(logger).setLevel(logLevel))
                .build();
    }

    @Provides
    CallAdapter.Factory provideCallAdapterFactories() {
        return RxJava2CallAdapterFactory.createAsync();
    }

    @Provides
    Converter.Factory provideConverterFactories(Moshi moshi) {
        return MoshiConverterFactory.create(moshi);
    }

    @Provides
    Retrofit provideRetrofit(
            @Named("baseApiUrl") String baseUrl,
            //Get call and converter factories
            //We can also provide list of factories if in future
            //we need more of them at once
            CallAdapter.Factory callAdapterFactory,
            Converter.Factory converterFactory,
            OkHttpClient okHttpClient
    ) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .addCallAdapterFactory(callAdapterFactory)
                .addConverterFactory(converterFactory)
                .build();
    }
}
