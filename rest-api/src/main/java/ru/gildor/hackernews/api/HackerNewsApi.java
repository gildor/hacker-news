package ru.gildor.hackernews.api;

import io.reactivex.Observable;
import ru.gildor.hackernews.api.models.Comment;
import ru.gildor.hackernews.api.models.Story;

import java.util.List;

public interface HackerNewsApi {
    Observable<List<Story>> loadTopStories(int limit);

    Observable<List<Comment>> loadStoryComments(Story story);

    Observable<List<Comment>> loadStoryComments(List<Long> commentIds);

    Observable<Story> loadStory(long storyId);

    Observable<Comment> loadComment(long commentId);

    Observable<List<Comment>> loadChildComments(Comment comment);
}
