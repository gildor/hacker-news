package ru.gildor.hackernews.api.test;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class TestResourceUtils {
    @SuppressWarnings("ConstantConditions")
    public static String loadResource(ClassLoader classLoader, String fileName) {

        StringBuilder result = new StringBuilder("");

        File file = new File(classLoader.getResource(fileName).getFile());
        Scanner scanner = null;
        try {
            scanner = new Scanner(file);

            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                result.append(line).append("\n");
            }

            scanner.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            if (scanner != null) {
                scanner.close();
            }
        }

        return result.toString();

    }
}
