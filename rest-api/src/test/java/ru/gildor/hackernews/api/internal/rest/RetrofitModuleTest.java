package ru.gildor.hackernews.api.internal.rest;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import org.junit.Before;
import org.junit.Test;
import retrofit2.CallAdapter;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.moshi.MoshiConverterFactory;
import ru.gildor.hackernews.api.logging.LogLevel;
import ru.gildor.hackernews.api.logging.Logger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class RetrofitModuleTest {
    private RetrofitModule retrofitModule;

    @Before
    public void setUp() throws Exception {
        retrofitModule = new RetrofitModule();
    }

    @Test
    public void provideApiUrl() throws Exception {
        assertEquals(
                RetrofitModule.API_ENDPOINT,
                retrofitModule.provideApiUrl()
        );
    }

    @Test
    public void provideLogLevel() throws Exception {
        assertEquals(
                HttpLoggingInterceptor.Level.BODY,
                retrofitModule.provideLogLevel(LogLevel.ALL)
        );
        assertEquals(
                HttpLoggingInterceptor.Level.BASIC,
                retrofitModule.provideLogLevel(LogLevel.BASIC)
        );
        assertEquals(
                HttpLoggingInterceptor.Level.NONE,
                retrofitModule.provideLogLevel(LogLevel.NONE)
        );
    }

    @Test(expected = NullPointerException.class)
    public void provideNullLogLevel() throws Exception {
        retrofitModule.provideLogLevel(null);
    }

    @Test
    public void provideLogger() throws Exception {
        Logger logger = mock(Logger.class);
        HttpLoggingInterceptor.Logger httpLogger = retrofitModule.provideLogger(logger);
        httpLogger.log("hi!");
        verify(logger).log("hi!");
    }

    @SuppressWarnings("SuspiciousMethodCalls")
    @Test
    public void provideOkHttpClient() throws Exception {
        HttpLoggingInterceptor.Logger logger = mock(HttpLoggingInterceptor.Logger.class);
        OkHttpClient okHttpClient = retrofitModule.provideOkHttpClient(logger, HttpLoggingInterceptor.Level.BASIC);
        assertTrue(okHttpClient.newBuilder().interceptors().get(0) instanceof HttpLoggingInterceptor);
    }

    @Test
    public void provideCallAdapterFactories() throws Exception {
        assertTrue(retrofitModule.provideCallAdapterFactories() instanceof RxJava2CallAdapterFactory);
    }

    @Test
    public void provideConverterFactories() throws Exception {
        assertTrue(retrofitModule.provideConverterFactories(retrofitModule.provideMoshi()) instanceof MoshiConverterFactory);
    }

    @Test
    public void provideRetrofit() throws Exception {
        String url = retrofitModule.provideApiUrl();
        Converter.Factory converterFactories = retrofitModule.provideConverterFactories(retrofitModule.provideMoshi());
        CallAdapter.Factory callAdapterFactory = retrofitModule.provideCallAdapterFactories();
        Retrofit retrofit = retrofitModule.provideRetrofit(
                url,
                callAdapterFactory,
                converterFactories,
                new OkHttpClient()
        );
        assertEquals(url, retrofit.baseUrl().toString());
        assertTrue(retrofit.converterFactories().contains(converterFactories));
        assertTrue(retrofit.callAdapterFactories().contains(callAdapterFactory));

    }

}