package ru.gildor.hackernews.api.models;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TypeTest {
    @Test
    public void valueOf() throws Exception {
        //Actually we do that for code coverage
        assertEquals(BaseStory.Type.STORY, BaseStory.Type.valueOf("STORY"));
    }
}