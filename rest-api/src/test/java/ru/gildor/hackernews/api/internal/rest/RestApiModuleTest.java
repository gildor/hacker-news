package ru.gildor.hackernews.api.internal.rest;

import io.reactivex.Single;
import org.junit.Assert;
import org.junit.Test;
import retrofit2.Retrofit;
import ru.gildor.hackernews.api.HackerNewsApi;
import ru.gildor.hackernews.api.internal.rest.models.StoryImpl;
import ru.gildor.hackernews.api.models.BaseStory;

import java.util.Collections;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class RestApiModuleTest {
    @Test
    public void provideHackerNewsService() throws Exception {
        Retrofit spy = new Retrofit.Builder().baseUrl(RetrofitModule.API_ENDPOINT).build();
        HackerNewsService hackerNewsService = new RestApiModule().provideHackerNewsService(spy);
        Assert.assertNotNull(hackerNewsService);

    }

    @Test
    public void provideHackerNewsApi() throws Exception {
        HackerNewsService service = mock(HackerNewsService.class);
        StoryImpl story = StoryImpl.create(
                100,
                "Story author",
                Collections.<Long>emptyList(),
                1231231322L,
                BaseStory.Type.STORY,
                "title",
                30,
                42,
                "story://url"
        );
        when(service.getStory(100)).thenReturn(
                Single.just(story)
        );
        HackerNewsApi api = new RestApiModule().provideHackerNewsApi(service);
        Assert.assertEquals(story, api.loadStory(100).blockingFirst());
    }

}