package ru.gildor.hackernews.api.internal.rest.models;

import com.squareup.moshi.Moshi;
import org.junit.Assert;
import org.junit.Test;
import ru.gildor.hackernews.api.internal.rest.TestRetrofitModule;
import ru.gildor.hackernews.api.test.TestResourceUtils;

public class StoryImplTest {
    @Test
    public void typeAdapter() throws Exception {
        String json = TestResourceUtils.loadResource(
                getClass().getClassLoader(),
                "story.json"
        );
        Moshi moshi = new TestRetrofitModule().provideMoshi();
        StoryImpl story = moshi.adapter(StoryImpl.class).fromJson(json);
        Assert.assertNotNull(story);
        Assert.assertEquals(14563118, story.id());
    }

}