package ru.gildor.hackernews.api.internal.rest.models;

import com.squareup.moshi.Moshi;
import org.junit.Assert;
import org.junit.Test;
import ru.gildor.hackernews.api.internal.rest.TestRetrofitModule;
import ru.gildor.hackernews.api.test.TestResourceUtils;

public class CommentImplTest {
    @Test
    public void typeAdapter() throws Exception {
        String json = TestResourceUtils.loadResource(
                getClass().getClassLoader(),
                "comment.json"
        );
        Moshi moshi = new TestRetrofitModule().provideMoshi();
        CommentImpl comment = moshi.adapter(CommentImpl.class).fromJson(json);
        Assert.assertNotNull(comment);
        Assert.assertEquals(2922097, comment.id());
    }

}
