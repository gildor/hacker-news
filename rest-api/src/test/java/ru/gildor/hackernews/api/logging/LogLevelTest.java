package ru.gildor.hackernews.api.logging;

import org.junit.Assert;
import org.junit.Test;

public class LogLevelTest {
    @Test
    public void valueOf() throws Exception {
        Assert.assertEquals(LogLevel.BASIC, LogLevel.valueOf("BASIC"));
    }
}