package ru.gildor.hackernews.api.internal.rest;

import okhttp3.logging.HttpLoggingInterceptor;
import org.junit.Before;
import org.junit.Test;
import ru.gildor.hackernews.api.logging.LogLevel;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class LoggingModuleTest {

    private LoggingModule module;

    @Before
    public void setUp() throws Exception {
        module = new LoggingModule();
    }

    @Test
    public void provideHttpLogger() throws Exception {
        assertEquals(HttpLoggingInterceptor.Logger.DEFAULT, module.provideHttpLogger());
    }

    @Test
    public void provideLogger() throws Exception {
        HttpLoggingInterceptor.Logger logger = mock(HttpLoggingInterceptor.Logger.class);
        module.provideLogger(logger).log("Log message");
        verify(logger).log("Log message");
    }

    @Test
    public void provideLogLevel() throws Exception {
        assertEquals(LogLevel.BASIC, module.provideLogLevel());
    }

}