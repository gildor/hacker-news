package ru.gildor.hackernews.api.internal.rest;

import io.reactivex.Single;
import org.junit.Before;
import org.junit.Test;
import ru.gildor.hackernews.api.internal.rest.models.CommentImpl;
import ru.gildor.hackernews.api.internal.rest.models.StoryImpl;
import ru.gildor.hackernews.api.models.BaseStory;
import ru.gildor.hackernews.api.models.Comment;
import ru.gildor.hackernews.api.models.Story;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class HackerNewsRestApiTest {

    private HackerNewsService service;
    private HackerNewsRestApi api;
    private List<Story> stories = Arrays.<Story>asList(
            createStory(100),
            createStory(200),
            createStory(300)
    );
    private List<Comment> comments = Arrays.<Comment>asList(
            createComment(1000),
            createComment(2000),
            createComment(3000)
    );

    @Before
    public void setUp() throws Exception {
        service = mock(HackerNewsService.class);
        when(service.getTopStories()).thenReturn(Single.just(
                Arrays.asList(100, 200, 300)
        ));
        initStoriesMock();

        initCommentsMock();

        api = new HackerNewsRestApi(service);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void loadTopStories() throws Exception {
        api.loadTopStories(10)
                .test()
                .assertComplete()
                .assertResult(stories);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void loadTopStoriesWithFilter() throws Exception {
        stories = Arrays.<Story>asList(
                createStory(100),
                createStory(201, Collections.<Long>emptyList(), BaseStory.Type.COMMENT),
                createStory(300)
        );
        initStoriesMock();

        ArrayList<Story> filtered = new ArrayList<>(this.stories);
        filtered.remove(1);
        api.loadTopStories(10)
                .test()
                .assertComplete()
                .assertResult(filtered);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void loadStoryComments() throws Exception {
        api.loadStoryComments(stories.get(0))
                .test()
                .assertComplete()
                .assertResult(comments);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void loadStoryCommentsWithDeletedItem() throws Exception {
        comments = Arrays.<Comment>asList(
                createComment(1000),
                createComment(2000, true),
                createComment(3000)
        );
        initCommentsMock();

        ArrayList<Comment> filtered = new ArrayList<>(comments);
        filtered.remove(1);
        api.loadStoryComments(createStory(100))
                .test()
                .assertComplete()
                .assertResult(filtered);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void loadEmptyStoryComments() throws Exception {
        api.loadStoryComments(createStory(100, null))
                .test()
                .assertComplete()
                .assertResult(Collections.<Comment>emptyList());
    }

    @Test
    public void loadStory() throws Exception {
        api.loadStory(100).test()
                .assertResult(stories.get(0));

        api.loadStory(200).test()
                .assertResult(stories.get(1));
    }

    @Test
    public void loadComment() throws Exception {
        api.loadComment(1000).test()
                .assertResult(comments.get(0));
        api.loadComment(2000).test()
                .assertResult(comments.get(1));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void loadChildComments() throws Exception {
        api.loadChildComments(comments.get(0)).test()
                .assertResult(comments);
    }

    private StoryImpl createStory(int id) {
        return createStory(id, Arrays.asList(1000L, 2000L, 3000L));
    }

    private StoryImpl createStory(int id, List<Long> kids) {
        return createStory(id, kids, BaseStory.Type.STORY);
    }

    private StoryImpl createStory(int id, List<Long> kids, BaseStory.Type type) {
        return StoryImpl.create(
                id,
                "Story author",
                kids,
                1231231322L,
                type,
                "title",
                30,
                42,
                "story://url"
        );
    }

    private CommentImpl createComment(int id, boolean deleted) {
        return CommentImpl.create(
                id,
                "Story author",
                Arrays.asList(1000L, 2000L, 3000L),
                1231231322L,
                0L,
                "text",
                deleted
        );
    }

    private CommentImpl createComment(int id) {
        return createComment(id, false);
    }

    private void initStoriesMock() {
        when(service.getStory(100L))
                .thenReturn(Single.just((StoryImpl) stories.get(0)));
        when(service.getStory(200L))
                .thenReturn(Single.just((StoryImpl) stories.get(1)));
        when(service.getStory(300L))
                .thenReturn(Single.just((StoryImpl) stories.get(2)));
    }

    private void initCommentsMock() {
        when(service.getComment(1000L))
                .thenReturn(Single.just((CommentImpl) comments.get(0)));
        when(service.getComment(2000L))
                .thenReturn(Single.just((CommentImpl) comments.get(1)));
        when(service.getComment(3000L))
                .thenReturn(Single.just((CommentImpl) comments.get(2)));
    }

}